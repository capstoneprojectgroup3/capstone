
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class sqltester {
	
	public static void main (String args []) throws Exception{

		Connection conn = null;

		try {
		conn =DriverManager.getConnection("jdbc:mysql://199.15.251.155:3306/capstonetest?user=group3&password=haskellisking");

		} catch (SQLException ex) {
		// handle any errors
		System.out.println("SQLException: " + ex.getMessage());
		System.out.println("SQLState: " + ex.getSQLState());
		System.out.println("VendorError: " + ex.getErrorCode());
		}

		Statement stmt = null;
		ResultSet rs = null;
		try {
		stmt = conn.createStatement();
		rs = stmt.executeQuery("SELECT Players FROM game");
		while (rs.next()) {
            String p = rs.getString("Players");
            System.out.println(p);
		}
		}
		catch (SQLException ex){
		// handle any errors
		System.out.println("SQLException: " + ex.getMessage());
		System.out.println("SQLState: " + ex.getSQLState());
		System.out.println("VendorError: " + ex.getErrorCode());
		}
		finally {
		// it is a good idea to release
		// resources in a finally{} block
		// in reverse-order of their creation
		// if they are no-longer needed
		if (rs != null) {
		try {
		rs.close();
		} catch (SQLException sqlEx) { } // ignore
		rs = null;
		}
		if (stmt != null) {
		try {
		stmt.close();
		} catch (SQLException sqlEx) { } // ignore
		stmt = null;
		}
		}
		
	}
}
