/**
 *
 * @author Marcel Kroll
 */
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqlConnection {
    
    public void main (String args []) throws Exception{
        System.out.println(this.GetAll());
        //System.out.println(this.GetSpecific("NAME", "registered_users", "UNIQUEID LIKE '435j34nhrjn3l453j45hnj45h'"));
    }
    
    
    public  Connection ConnectToDb () throws Exception{
        String url = "jdbc:mysql://l1ght.net:3306/";
        
        String dbName = "capstonetest";
        
        String driver = "com.mysql.jdbc.Driver";
        
        String userName = "group3";
        
        String password = "haskellisking";
        
        try{
            Class.forName(driver).newInstance();
            
            Connection conn = DriverManager.getConnection(url + dbName, userName, password);
            
            return conn;

        }
        
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
        
        
    }
    public List GetAll () throws Exception {
        
        Connection conn  = this.ConnectToDb();
        
        List returnList = new ArrayList();
        
        Statement statement = conn.createStatement();
        
        ResultSet res = statement.executeQuery("SELECT players FROM game");
        
        while (res.next()){
            List temp = new ArrayList();
            
            //int id = res.getInt("ID");
            String name = res.getString("NAME");
            //String uid = res.getString("UNIQUEID");
            //String winloss = res.getString("WINLOSS");
            
           // temp.add(id);
            temp.add(name);
           // temp.add(uid);
            //temp.add(winloss);
            
            returnList.add(temp);         
                          }        
        conn.close();
        
        return returnList;
    }
    
    public  List GetSpecific (String selection, String table, String whereClause)throws Exception{
        
        Connection conn = this.ConnectToDb();

        List returnList = new ArrayList();
        
        Statement statement = conn.createStatement();
        
        ResultSet res = statement.executeQuery("SELECT " + selection + " FROM " + table + " WHERE " + whereClause);
        System.out.println(res);
        
        while (res.next()){
            returnList.add(res.getString(selection));
        }
        
        conn.close();
        return returnList;
        
    }
}
