// My information
var me = {myId:0 ,myGameName:'', myColor:''};
// END My information

// Global variables
var gameStarted = 0; // flag to indicate if users can use map
var gameSetupPhase = 1; // flag to indicate whether game in place army or attack mode

var gameId = 0; // INT - the id of this specific game
var gameName = ''; // the name of this specific game
var playersDict = [];
var listOfUsers = []; // list of the users in the game - stored as sessionId's

var myTurn = 0;
var firstTurn = 1;
var firstCountry;

var colors = ["blue","red","green","orange","purple","yellow"];
var unitCount = 4;
//END Global variables

// Functions activated by selecting map	-	-	-	-	-	-	-	-	-	-	-	-	-
function myFunc(caller){ // when you click on the map this function is called
	if(gameStarted){
		if(gameSetupPhase){
			if(myTurn&&window[caller.id].owner==me.myId){
				unitCount -=1;
				if(unitCount==0){
					gameSetupPhase = 0; 
					document.getElementById("deployDiv").style.visibility="hidden";					
					document.getElementById("gameStage").value = "ATTACK";
					document.getElementById("gameStage").style.color = "#F30021";
				}
				document.getElementById("deployMeter").value = unitCount;
				placeUnit(caller.id);
			}
		} else if(myTurn){ // its my turn and I can attack or reinforce
			if(firstTurn){
				firstChoice(caller);
			} else {
				if(caller.id==firstCountry.id){
					deselectCountry(firstCountry);
					firstTurn = 1;
				} else {
					secondChoice(caller);
				}
			}
		}
	}
};

function firstChoice(caller){
	if(window[caller.id].owner==me.myId){ 
		if(window[caller.id].unitCount>1){
			selectCountry(caller);
			firstTurn = 0; // its not my first turn anymore
			firstCountry = caller; // remember my first cell choice 
		}            
	}       
};

function secondChoice(caller){
	// check to make sure I havn't already selected this country
	if(window[caller.id].owner!=me.myId){ // if true then I must be attacking
		console.log("im trying to attack");
		if(window[firstCountry.id].neighbours.indexOf(caller.id)>=0){// check whether second selected country is neighbour
			console.log("Second country is a neighbour");
			if(window[firstCountry.id].unitCount >=2){//but to attack, I need at least two armies
				deselectCountry(firstCountry); // change the first selected country back to my color	
				firstTurn = 1;					
				attack(firstCountry,caller);
			}
		}
	} else { // I must be reinforcing
		if(window[firstCountry.id].neighbours.indexOf(caller.id)>=0){// check whether second selected country is neighbour
			console.log("The country I want to reinforce is a neighbour");
			selectCountry(caller);
			var max = window[firstCountry.id].unitCount - 1;
			var n = -1;
			while(n<0){
				n = window.prompt("How many armies would you like to move? (Max: " + max,"");
				if(n>max){
					n = -1;
				}
			}
			if(n==0){
				deselectCountry(caller);
				deselectCountry(firstCountry);
				firstTurn = 1;
			} else {
				deselectCountry(caller);
				deselectCountry(firstCountry);
				reinforce(firstCountry,caller,n);
			}	
		}
	}
};
function selectCountry(caller)
{
	caller.setAttribute("fill", "grey");
						//TODO 
		// maybe make this animate/fade in and out
};
function deselectCountry(caller)
{
	caller.setAttribute("fill", me.myColor);
};

// END Functions activated by selecting map	-	-	-	-	-	-	-	-	-	-	-

// Chat handling function
function chathandler(msg) {
	console.log('chat: ' + msg);
}

// Lobby events handling (new user and game event that need to be shown in interface
function lobbyhandler(evt) {
    console.log('lobby: ' + evt);
}

// Websockets game functions---------------------------------------------------------

function gamehandler(response){ // function to receive data from server 

     if(response[0]==="join"){
        var nothing = 0; // This code does nothing. This will be implemented later as part of lobby
		console.log('join event recieved');
    } else if(response[0][0]==="startgame"){
        playersDict = response[0][1]
		console.log('player: ' + playersDict);
        listOfUsers = [];
		for (p=0;p<playersDict.length;p++) {
			if (p%2==0) {
				listOfUsers.add(playersDict[p]);
			}
		}
        var cIndex = listOfUsers.indexOf(me.myGameName);
        me.myColor = colors[cIndex];
        document.getElementById("displayMyColour").value = me.myColor.toUpperCase();
        document.getElementById("displayMyColour").style.color = me.myColor;
        document.getElementById("displayMyName").style.color = me.myColor;
		document.getElementById("startGameButton").style.visibility="hidden";
        getGameState(); // calls initgamestate
    } else if(response[0]==="initgamestate"){ // 
        setUpMap(response[1]);
        if(response[2]==me.myId){
            myTurn = 1;
        }
        gameStarted = 1;
        currentPlayerName = playersDict[response[2]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[2]])];
        document.getElementById("gameStage").value = "Place Armies";
        document.getElementById("gameStage").style.color = "#FF7100";
    } else if(response[0]==="placeunit"){
        var country = response[1];
        var unitQ = parseInt(response[2]);
		window[country].unitCount += unitQ;
        if(response[3]==me.myId){
            myTurn = 1;
        } else {
            myTurn = 0;
        }
        document.getElementById("displayCurrentPlayer").value = playersDict[response[3]].toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[3]])];
    } else if(response[0]==="turn"){
        if(response[1]==me.myId){
            myTurn = 1;
        } else {
            myTurn = 0;
        }
        currentPlayerName = playersDict[response[1]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[1]])];
    } else if(response[0]=="reinforce"){
		console.log(response);
		console.log("The above is received from server on reinforce");
        var countryFrom = response[1];
        var countryTo = response[2];
        var unitQuantity = parseInt(response[3]);
        window[countryFrom].unitCount -= unitQuantity;
		console.log(countryFrom);
		console.log(countryTo);
        window[countryTo].unitCount += unitQuantity;
    } else if(response[0]==="attack"){
        var moveStats = response[1].split(',');
        var attackingCountry = moveStats[0];
        var defendingCountry = moveStats[1];
		var attackerLoss = parseInt(moveStats[2]);
		var defenderLoss = parseInt(moveStats[3]);

        if (window[defendingCountry].unitCount-defenderLoss <= 0) {//If attacker won, the defender will have 0 or -1 armies left!
            window[defendingCountry].owner = window[attackingCountry].owner;// defenders country now owned by attacker
            window[defendingCountry].unitCount = window[attackingCountry].unitCount-1+parseInt(moveStats[2]); // units moved to new country
            window[attackingCountry].unitCount = 1; // attacking country now has 1 unit
            document.getElementById(moveStats[1]).setAttribute("fill",colors[listOfUsers.indexOf(playersDict[window[attackingCountry].owner])]);//and set the colours
            firstTurn = 1; //you can continue to attack now
        } else {
            window[defendingCountry].unitCount -= defenderLoss;
			window[attackingCountry].unitCount -= attackerLoss;
        }
    } else if(response[0]==="gamefiished"){// < -- PURPOSELY DISABLED FOR NOW
        if(response[2]==gameId){
            gameStatus = 0;
            if(response[3]==me.myId){
                alert("YOU are the victor !");
            } else {
                alert("Player "+response[3]+" has won the game :-(");
            }
        }
    } else { //if we haven't interpreted the message at all, tell us about it
            console.log("Client needs to handle this:" +response);
	}
};


//Send to server functions	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
//The returns in these functions are there for use in the unit tests
function getLobbyList(){
    console.log("woah look here");
    session.call('com.example.lobbylist', [me.myId]).then(
                           function(res){                                                           
                               console.log("this is a test: " + res);                
                               }                           
                );
}
function newUser(name,emailAddy){
    document.getElementById("displayMyName").value = name.toUpperCase();
    me.myGameName = name;
	session.call('com.example.newuser', [name,emailAddy]).then(
					   function (res) {
							me.myId = res[0];
							console.log("newuser response:", res);
							//document.getElementById("newUserButton").style.visibility="hidden";						
                                                        getLobbyList();
                                           })
					};
                                        
function newGame(gameName){

   session.call('com.example.newgame', [me.myId,gameName]).then(
				   function (res) {
						gameId = res[0];
						console.log("newgame response:", res);
						mount='com.example.'+gameId;
						session.subscribe(mount,gamehandler)
						console.log("subscribed to game channel: " + mount)	
						document.getElementById("newGameButton").style.visibility="hidden";	
						$("#joinGameButton").hide();						
				   })
				};

function joinGame(gname){
    if(gameStarted==0){
        session.call('com.example.joingame', [me.myId,gname]).then(
				   function (res) {
						gameId = res[0];
						gameName = res[1];
						console.log("joingame response:", res);
						mount='com.example.'+gameId;
						session.subscribe(mount,gamehandler)
						console.log("subscribed to game channel: " + mount)	
						$("#newGameButton").hide();	
						$("#joinGameButton").hide();		
						$("#startGameButton").hide();								
				   })
				};
};
function startGame(){
    var gId = gameId.toString();
	session.call('com.example.startgame', [me.myId,gId]);
};

function getGameState(){ //initgamestate
    var gId = gameId.toString();
	session.call('com.example.initgamestate', [me.myId,gId]);
	
};

function placeUnit(countryPlaced){
    var gId = gameId.toString();
    myTurn = 0;
    if(isopen){
        socket.send("placeunit;"+me.myId+";"+gId+";"+countryPlaced+",1");
		return 1;
    } else {
        console.log("Connection not open");
		return 0;
    }
};

function attack(countryA,countryB) {
    var gId = gameId.toString();
    if (isopen) {
        console.log("attack;"+me.myId+";"+gId+";"+countryA.id+','+countryB.id);
        socket.send("attack;"+me.myId+";"+gId+";"+countryA.id+','+countryB.id);
	   return 1;
    } else {
       console.log("Connection not opened.")
	   return 0;
    }
 };
 
 function reinforce(originCountry,countryToReinforce,unitQuantity) {
    var gId = gameId.toString();
    var UQ = unitQuantity.toString();
	console.log(UQ);
    if (isopen) {
       socket.send("reinforce;"+me.myId+";"+gId+";"+originCountry.id+','+countryToReinforce.id+','+UQ);
       console.log("reinforce;"+me.myId+";"+gId+";"+originCountry.id+','+countryToReinforce.id+','+UQ);
	   return 1;
    } else {
       console.log("Connection not opened.")
	   return 0;
    }
 };
 
 function endTurn() {
    if(gameSetupPhase){
        alert("Place your units");
        return;
    } else {
        myTurn = 0;
        var gId = gameId.toString();
        if (isopen) {
            socket.send("turnend;"+me.myId+";"+gId+";None");
            return 1;
        } else {
            console.log("Connection not opened.");
            return 0;
        }
    }
 };
// END Send to server functions	-	-	-	-	-	-	-	-	-	-	-	-	-
window.onload = function() {
         var connection = new autobahn.Connection({
            url: "ws://127.0.0.1:8080",
            realm: 'realm1'
         });
			

         connection.onopen = function (ses) {
			session = ses;
            console.log("connected");
			
			function onchat(msg) {
				console.log("CHAT: " + msg);
				$('#chat').html(msg);
			}
			session.subscribe('com.example.chat', chathandler);
			session.subscribe('com.example.lobby', lobbyhandler);
			
        };
         connection.open();
};
// end Web Socket Setup--------------------------------------------------------

function setUpMap(state){ // receive players map distribution from server and update country info
    var x = state.split(',');
    var i = 0;
    for(i=0;i<42;i++){   
        window[countryList[i]].owner = x[i];
        var newColor = colors[listOfUsers.indexOf(playersDict[x[i]])];
        document.getElementById(countryList[i]).setAttribute("fill",newColor);
    }     
};

// Country information		-		-		-		-		-		-		-		-		-		-		-		-		-		-		-		
countryList = ['afghanistan', 'alaska', 'alberta', 'argentina', 'australia', 'brazil', 'centralamerica', 'china', 'congo', 'eastafrica', 'easternrussia', 'easternunitedstates', 'egypt', 'greatbritain', 'greenland', 'iceland', 'india', 'indonesia', 'japan', 'kamchatka', 'madagascar', 'middleeast', 'mongolia', 'newguinea', 'newzealand', 'northafrica', 'northerneurope', 'northernterritory', 'ontario', 'peru', 'quebec', 'russia', 'scandinavia', 'siberia', 'southafrica', 'southerneurope', 'thailand', 'ural', 'venezuela', 'westerneurope', 'westernunitedstates', 'yakutsk'];
var afghanistan = {owner:'', unitCount:1, neighbours: ['middleeast', 'china', 'ural', 'russia', 'india'] };
var alaska = {owner:'', unitCount:1, neighbours: ['northernterritory', 'alberta'] };
var alberta = {owner:'', unitCount:1, neighbours: ['alaska', 'northernterritory', 'ontario', 'westernunitedstates'] };
var argentina = {owner:'', unitCount:1, neighbours: ['peru', 'brazil'] };
var australia = {owner:'', unitCount:1, neighbours: ['newguinea', 'indonesia', 'newzealand'] };
var brazil = {owner:'', unitCount:1, neighbours: ['venezuela', 'peru', 'argentina'] };
var centralamerica = {owner:'', unitCount:1, neighbours: ['venezuela', 'westernunitedstates', 'easternunitedstates'] };
var china = {owner:'', unitCount:1, neighbours: ['thailand', 'india', 'afghanistan', 'ural', 'siberia', 'mongolia'] };
var congo = {owner:'', unitCount:1, neighbours: ['northafrica', 'eastafrica', 'southafrica'] };
var eastafrica = {owner:'', unitCount:1, neighbours: ['egypt', 'northafrica', 'southafrica', 'congo'] };
var easternrussia = {owner:'', unitCount:1, neighbours: ['mongolia', 'yakutsk', 'kamchatka', 'siberia'] };
var easternunitedstates = {owner:'', unitCount:1, neighbours: ['centralamerica', 'westernunitedstates', 'ontario', 'quebec'] };
var egypt = {owner:'', unitCount:1, neighbours: ['middleeast', 'norhtafrica', 'eastafrica'] };
var greatbritain = {owner:'', unitCount:1, neighbours: ['iceland', 'northerneurope', 'westerneurope'] };
var greenland = {owner:'', unitCount:1, neighbours: ['quebec', 'iceland'] };
var iceland = {owner:'', unitCount:1, neighbours: ['greenland', 'greatbritain', 'scandinavia'] };
var india = {owner:'', unitCount:1, neighbours: ['thailand', 'china', 'afghanastan', 'middleeast'] };
var indonesia = {owner:'', unitCount:1, neighbours: ['australia', 'newguinea', 'thailand'] };
var japan = {owner:'', unitCount:1, neighbours: ['china', 'kamchatka'] };
var kamchatka = {owner:'', unitCount:1, neighbours: ['yakutsk', 'easternrussia', 'mongolia', 'japan'] };
var madagascar = {owner:'', unitCount:1, neighbours: ['southafrica'] };
var middleeast = {owner:'', unitCount:1, neighbours: ['egypt', 'russia', 'afghanistan', 'india', 'southereurope'] };
var mongolia = {owner:'', unitCount:1, neighbours: ['china', 'siberia', 'easternrussia', 'kamchatka', 'japan'] };
var newguinea = {owner:'', unitCount:1, neighbours: ['indonesia', 'australia', 'thailand'] };
var newzealand = {owner:'', unitCount:1, neighbours: ['australia'] };
var northafrica = {owner:'', unitCount:1, neighbours: ['egypt', 'eastafrica', 'congo', 'westerneurope'] };
var northerneurope = {owner:'', unitCount:1, neighbours: ['westerneurope', 'southerneurope', 'russia', 'scandinavia'] };
var northernterritory = {owner:'', unitCount:1, neighbours: ['alberta', 'alaska', 'ontario'] };
var ontario = {owner:'', unitCount:1, neighbours: ['northernterritory', 'alberta', 'westernunitedstates', 'easternunitedstates', 'quebec'] };
var peru = {owner:'', unitCount:1, neighbours: ['venezuela', 'brazil', 'argentina'] };
var quebec = {owner:'', unitCount:1, neighbours: ['easternunitedstates', 'ontario', 'greenland'] };
var russia = {owner:'', unitCount:1, neighbours: ['middleeast', 'southerneurope', 'northerneurope', 'scandinavia', 'afghanistan', 'ural'] };
var scandinavia = {owner:'', unitCount:1, neighbours: ['iceland', 'northerneurope', 'russia'] };
var thailand = {owner:'', unitCount:1, neighbours: ['china', 'india','indonesia','newguinea'] };
var siberia = {owner:'', unitCount:1, neighbours: ['yakutsk', 'ural', 'china', 'mongolia', 'easternrussia'] };
var southafrica = {owner:'', unitCount:1, neighbours: ['congo', 'eastafrica', 'madagascar'] };
var southerneurope = {owner:'', unitCount:1, neighbours: ['westerneurope', 'northerneurope', 'middleeast', 'russia'] };
var ural = {owner:'', unitCount:1, neighbours: ['russia', 'afghanistan', 'china', 'siberia'] };
var venezuela = {owner:'', unitCount:1, neighbours: ['centralamerica', 'brazil', 'peru'] };
var westerneurope = {owner:'', unitCount:1, neighbours: ['greatbritain', 'northerneurope', 'southerneurope', 'northafrica'] };
var westernunitedstates = {owner:'', unitCount:1, neighbours: ['alberta', 'ontario', 'easternunitedstates', 'centralamerica'] };
var yakutsk = {owner:'', unitCount:1, neighbours: ['siberia', 'easternrussia', 'kamchatka'] };
// END Country information		-		-		-		-		-		-		-		-		-		-		-		-		-		-		-