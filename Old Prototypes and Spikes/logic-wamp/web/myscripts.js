// My information
var me = {myId:0 ,myGameName:'', myColor:''};
// END My information

// Global variables
var gameStarted = 0; // flag to indicate if users can use map
var gameSetupPhase = 1; // flag to indicate whether game in place army or attack mode

var gameId = 0; // INT - the id of this specific game
var gameName = ''; // the name of this specific game
var playersDict = [];
var listOfUsers = []; // list of the users in the game - stored as sessionId's

var myTurn = 0;
var firstTurn = 1;
var firstCountry;

var colors = ["blue","red","green","orange","purple","yellow"];
var unitCount = 0;
//END Global variables

// Functions activated by selecting map	-	-	-	-	-	-	-	-	-	-	-	-	-
function myFunc(caller){ // when you click on the map this function is called
	if(gameStarted){
		if(gameSetupPhase){
			if(myTurn&&window[caller.id].owner==me.myId){
				unitCount -=1;
				if(unitCount==0){
					gameSetupPhase = 0; 
					document.getElementById("deployDiv").style.visibility="hidden";					
					document.getElementById("gameStage").value = "ATTACK";
					document.getElementById("gameStage").style.color = "#F30021";
				}
				document.getElementById("deployMeter").value = unitCount;
				placeUnit(caller.id);
			}
		} else if(myTurn){ // its my turn and I can attack or reinforce
			if(firstTurn){
				firstChoice(caller);
			} else {
				if(caller.id==firstCountry.id){
					deselectCountry(firstCountry);
					firstTurn = 1;
				} else {
					secondChoice(caller);
				}
			}
		}
	}
};

function firstChoice(caller){
	if(window[caller.id].owner==me.myId){ 
		if(window[caller.id].unitCount>1){
			selectCountry(caller);
			firstTurn = 0; // its not my first turn anymore
			firstCountry = caller; // remember my first cell choice 
		}            
	}       
};

function secondChoice(caller){
	// check to make sure I havn't already selected this country
	if(window[caller.id].owner!=me.myId){ // if true then I must be attacking
		console.log("im trying to attack");
		if(window[firstCountry.id].neighbours.indexOf(caller.id)>=0){// check whether second selected country is neighbour
			console.log("Second country is a neighbour");
			if(window[firstCountry.id].unitCount >=2){//but to attack, I need at least two armies
				deselectCountry(firstCountry); // change the first selected country back to my color	
				firstTurn = 1;					
				attack(firstCountry.id,caller.id);
			}
		}
	} else { // I must be reinforcing
		if(window[firstCountry.id].neighbours.indexOf(caller.id)>=0){// check whether second selected country is neighbour
			console.log("The country I want to reinforce is a neighbour");
			selectCountry(caller);
			var max = window[firstCountry.id].unitCount - 1;
			var n = -1;
			while(n<0){
				n = parseInt(window.prompt("How many armies would you like to move? (Max: " + max,""));
				if(isNaN(n) || n > max){
					n = -1;
				}
			}
			if(n==0){
				deselectCountry(caller);
				deselectCountry(firstCountry);
				firstTurn = 1;
			} else {
				deselectCountry(caller);
				deselectCountry(firstCountry);
				reinforce(firstCountry.id,caller.id,n);
				firstTurn = 1;
				endTurn(); //can only reinforce as final action in the turn
			}	
		}
	}
};
function selectCountry(caller)
{
	caller.setAttribute("fill", "grey");
						//TODO 
		// maybe make this animate/fade in and out
};
function deselectCountry(caller)
{
	caller.setAttribute("fill", me.myColor);
};

// END Functions activated by selecting map	-	-	-	-	-	-	-	-	-	-	-

// Chat handling function
function chathandler(msg) {
	console.log('chat: ' + msg);
}

// Lobby events handling (new user and game event that need to be shown in interface
function lobbyhandler(evt) {
    console.log('lobby: ' + evt);
}

// Websockets game functions---------------------------------------------------------

function gamehandler(response){ // function to receive data from server 
	console.log(response);
     if(response[0]==="join"){
        var nothing = 0; // This code does nothing. This will be implemented later as part of lobby
		console.log('join event recieved');
    } else if(response[0][0]==="startgame"){
		//fill the player dictionary
		//response[0][1] is an array of n players, each player is a two index array of name and id
		for (i = 0; i < response[0][1].length; i++){
			playersDict[response[0][1][i][0]] = response[0][1][i][1];
		}
		console.log('player: ' + playersDict);
        listOfUsers = [];
		for (p in playersDict) {
				listOfUsers.push(playersDict[p]);
		}
        var cIndex = listOfUsers.indexOf(me.myGameName);
        me.myColor = colors[cIndex];
        document.getElementById("displayMyColour").value = me.myColor.toUpperCase();
        document.getElementById("displayMyColour").style.color = me.myColor;
        document.getElementById("displayMyName").style.color = me.myColor;
		document.getElementById("startGameButton").style.visibility="hidden";
        getGameState(); // calls initgamestate
    } else if(response[0]==="initgamestate"){ 
		//response = ['initgamestate',[list of ids corresponding to armies],firstPlayer,startingUnits] 
        setUpMap(response[1]);
		unitCount = response[3];
        if(response[2]==me.myId){
            myTurn = 1;
        }
        gameStarted = 1;
        currentPlayerName = playersDict[response[2]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[2]])];
        document.getElementById("gameStage").value = "Place Armies";
        document.getElementById("gameStage").style.color = "#FF7100";
    } else if(response[0]==="placeunit"){
        var country = response[1];
        //var unitQ = parseInt(response[2]);
		//always only add one unit
		//response2 is nextuser
		//window[country].unitCount += unitQ;
		window[country].unitCount += 1;
        if(response[2]==me.myId){
            myTurn = 1;
        } else {
            myTurn = 0;
        }
        document.getElementById("displayCurrentPlayer").value = playersDict[response[2]].toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[2]])];
    } else if(response[0]==="turn"){
        if(response[1]==me.myId){
            myTurn = 1;
        } else {
            myTurn = 0;
        }
        currentPlayerName = playersDict[response[1]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[1]])];
    } else if(response[0]=="reinforce"){
		console.log(response);
		console.log("The above is received from server on reinforce");
        var countryFrom = response[1];
        var countryTo = response[2];
        var unitQuantity = parseInt(response[3]);
        window[countryFrom].unitCount -= unitQuantity;
		console.log(countryFrom);
		console.log(countryTo);
        window[countryTo].unitCount += unitQuantity;
    } else if(response[0]==="attack"){
		//response = ["attack",from,to,fromresult,toresult]
        var attackingCountry = response[1];
        var defendingCountry = response[2];
		var attackerLoss = parseInt(response[3]);
		var defenderLoss = parseInt(response[4]);
		
        if (window[defendingCountry].unitCount-defenderLoss <= 0) {//If attacker won, the defender will have 0 or -1 armies left!
            window[defendingCountry].owner = window[attackingCountry].owner;// defenders country now owned by attacker
            window[defendingCountry].unitCount = (window[attackingCountry].unitCount - 1); // units moved to new country
            window[attackingCountry].unitCount = 1; // attacking country now has 1 unit
			var attackingPlayerColour = colors[listOfUsers.indexOf(playersDict[window[attackingCountry].owner])];
            document.getElementById(defendingCountry).setAttribute("fill",attackingPlayerColour);//and set the colours
        } else {
			window[attackingCountry].unitCount -= attackerLoss; //otherwise just update stats
            window[defendingCountry].unitCount -= defenderLoss; 
        }
		firstTurn = 1; //you can continue to attack now
    } else if(response[0]==="gamefiished"){// < -- PURPOSELY DISABLED FOR NOW
        if(response[2]==gameId){
            gameStatus = 0;
            if(response[3]==me.myId){
                alert("YOU are the victor !");
            } else {
                alert("Player "+response[3]+" has won the game :-(");
            }
        }
    } else { //if we haven't interpreted the message at all, tell us about it
            console.log("Client needs to handle this:" +response);
	}
};
function lobbylist(){
	console.log('me.myId is: ' + me.myId)
    session.call('com.example.lobbylist', ["" + me.myId]).then(
				   function (res) {
						console.log(res)
				   })
				};

// Send to server functions	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
//The returns in these functions are there for use in the unit tests
function newUser(name,emailAddy){
    document.getElementById("displayMyName").value = name.toUpperCase();
    me.myGameName = name;
	session.call('com.example.newuser', [name,emailAddy]).then(
					   function (res) {
							me.myId = res[0];
							console.log("newuser response:", res);
							document.getElementById("newUserButton").style.visibility="hidden";	
							lobbylist();
					   })
					};
function newGame(gameName){

   session.call('com.example.newgame', [me.myId,gameName]).then(
				   function (res) {
						gameId = res[0];
						console.log("newgame response:", res);
						mount='com.example.'+gameId;
						session.subscribe(mount,gamehandler)
						console.log("subscribed to game channel: " + mount)	
						document.getElementById("newGameButton").style.visibility="hidden";	
						$("#joinGameButton").hide();						
				   })
				};

				
				
function joinGame(gname){
    if(gameStarted==0){
        session.call('com.example.joingame', [me.myId,gname]).then(
				   function (res) {
						gameId = res[0];
						gameName = res[1];
						console.log("joingame response:", res);
						mount='com.example.'+gameId;
						session.subscribe(mount,gamehandler)
						console.log("subscribed to game channel: " + mount)	
						$("#newGameButton").hide();	
						$("#joinGameButton").hide();		
						$("#startGameButton").hide();								
				   })
				};
};
function startGame(){
    var gId = gameId.toString();
	session.call('com.example.startgame', [me.myId,gId]);
};

function getGameState(){ //initgamestate
    var gId = gameId.toString();
	session.call('com.example.initgamestate', [me.myId,gId]);
	
};

function placeUnit(countryPlaced){
    var gId = gameId.toString();
    myTurn = 0;
    session.call('com.example.placeunit', [me.myId,gId,countryPlaced]);
};

function attack(countryA,countryB) {
    var gId = gameId.toString();
	session.call('com.example.attack', [me.myId,gId,countryA,countryB]);
 };
 
 function reinforce(originCountry,countryToReinforce,unitQuantity) {
    var gId = gameId.toString();
	session.call('com.example.reinforce', [me.myId,gId,originCountry,countryToReinforce,unitQuantity]);	
 };
 
 function endTurn() {
    if(gameSetupPhase){
        alert("Place your units");
        return;
    } else {
		var gId = gameId.toString();
		session.call('com.example.turnend', [me.myId,gId]);
		myTurn = 0;
    }
 };
// END Send to server functions	-	-	-	-	-	-	-	-	-	-	-	-	-
window.onload = function() {
         var connection = new autobahn.Connection({
            url: "ws://127.0.0.1:8080",
            realm: 'realm1'
         });
			

         connection.onopen = function (ses) {
			session = ses;
            console.log("connected");
			
			function onchat(msg) {
				console.log("CHAT: " + msg);
				$('#chat').html(msg);
			}
			session.subscribe('com.example.chat', chathandler);
			session.subscribe('com.example.lobby', lobbyhandler);
			
        };
         connection.open();
};
// end Web Socket Setup--------------------------------------------------------

function setUpMap(state){ // receive players map distribution from server and update country info
    for(i=0;i<42;i++){   
        window[countryList[i]].owner = state[i];
        var newColor = colors[listOfUsers.indexOf(playersDict[state[i]])];
        document.getElementById(countryList[i]).setAttribute("fill",newColor);
    }     
};

// Country information		-		-		-		-		-		-		-		-		-		-		-		-		-		-		-		
countryList = ['afghanistan', 'alaska', 'alberta', 'argentina', 'australia', 'brazil', 'centralamerica', 'china', 'congo', 'eastafrica', 'easternrussia', 'easternunitedstates', 'egypt', 'greatbritain', 'greenland', 'iceland', 'india', 'indonesia', 'japan', 'kamchatka', 'madagascar', 'middleeast', 'mongolia', 'newguinea', 'newzealand', 'northafrica', 'northerneurope', 'northernterritory', 'ontario', 'peru', 'quebec', 'russia', 'scandinavia', 'siberia', 'southafrica', 'southerneurope', 'thailand', 'ural', 'venezuela', 'westerneurope', 'westernunitedstates', 'yakutsk'];
var afghanistan = {owner:'', unitCount:1, neighbours: ['middleeast', 'china', 'ural', 'russia', 'india'] };
var alaska = {owner:'', unitCount:1, neighbours: ['northernterritory', 'alberta'] };
var alberta = {owner:'', unitCount:1, neighbours: ['alaska', 'northernterritory', 'ontario', 'westernunitedstates'] };
var argentina = {owner:'', unitCount:1, neighbours: ['peru', 'brazil'] };
var australia = {owner:'', unitCount:1, neighbours: ['newguinea', 'indonesia', 'newzealand'] };
var brazil = {owner:'', unitCount:1, neighbours: ['venezuela', 'peru', 'argentina'] };
var centralamerica = {owner:'', unitCount:1, neighbours: ['venezuela', 'westernunitedstates', 'easternunitedstates'] };
var china = {owner:'', unitCount:1, neighbours: ['japan','thailand', 'india', 'afghanistan', 'ural', 'siberia', 'mongolia'] };
var congo = {owner:'', unitCount:1, neighbours: ['northafrica', 'eastafrica', 'southafrica'] };
var eastafrica = {owner:'', unitCount:1, neighbours: ['egypt', 'northafrica', 'southafrica', 'congo'] };
var easternrussia = {owner:'', unitCount:1, neighbours: ['mongolia', 'yakutsk', 'kamchatka', 'siberia'] };
var easternunitedstates = {owner:'', unitCount:1, neighbours: ['centralamerica', 'westernunitedstates', 'ontario', 'quebec'] };
var egypt = {owner:'', unitCount:1, neighbours: ['middleeast', 'northafrica', 'eastafrica'] };
var greatbritain = {owner:'', unitCount:1, neighbours: ['iceland', 'northerneurope', 'westerneurope'] };
var greenland = {owner:'', unitCount:1, neighbours: ['quebec', 'iceland'] };
var iceland = {owner:'', unitCount:1, neighbours: ['greenland', 'greatbritain', 'scandinavia'] };
var india = {owner:'', unitCount:1, neighbours: ['thailand', 'china', 'afghanistan', 'middleeast'] };
var indonesia = {owner:'', unitCount:1, neighbours: ['australia', 'newguinea', 'thailand'] };
var japan = {owner:'', unitCount:1, neighbours: ['china', 'kamchatka', 'mongolia'] };
var kamchatka = {owner:'', unitCount:1, neighbours: ['yakutsk', 'easternrussia', 'mongolia', 'japan'] };
var madagascar = {owner:'', unitCount:1, neighbours: ['southafrica'] };
var middleeast = {owner:'', unitCount:1, neighbours: ['egypt', 'russia', 'afghanistan', 'india', 'southerneurope'] };
var mongolia = {owner:'', unitCount:1, neighbours: ['china', 'siberia', 'easternrussia', 'kamchatka', 'japan'] };
var newguinea = {owner:'', unitCount:1, neighbours: ['indonesia', 'australia', 'thailand'] };
var newzealand = {owner:'', unitCount:1, neighbours: ['australia'] };
var northafrica = {owner:'', unitCount:1, neighbours: ['egypt', 'eastafrica', 'congo', 'westerneurope'] };
var northerneurope = {owner:'', unitCount:1, neighbours: ['greatbritain','westerneurope', 'southerneurope', 'russia', 'scandinavia'] };
var northernterritory = {owner:'', unitCount:1, neighbours: ['alberta', 'alaska', 'ontario'] };
var ontario = {owner:'', unitCount:1, neighbours: ['northernterritory', 'alberta', 'westernunitedstates', 'easternunitedstates', 'quebec'] };
var peru = {owner:'', unitCount:1, neighbours: ['venezuela', 'brazil', 'argentina'] };
var quebec = {owner:'', unitCount:1, neighbours: ['easternunitedstates', 'ontario', 'greenland'] };
var russia = {owner:'', unitCount:1, neighbours: ['middleeast', 'southerneurope', 'northerneurope', 'scandinavia', 'afghanistan', 'ural'] };
var scandinavia = {owner:'', unitCount:1, neighbours: ['iceland', 'northerneurope', 'russia'] };
var thailand = {owner:'', unitCount:1, neighbours: ['china', 'india','indonesia','newguinea'] };
var siberia = {owner:'', unitCount:1, neighbours: ['yakutsk', 'ural', 'china', 'mongolia', 'easternrussia'] };
var southafrica = {owner:'', unitCount:1, neighbours: ['congo', 'eastafrica', 'madagascar'] };
var southerneurope = {owner:'', unitCount:1, neighbours: ['westerneurope', 'northerneurope', 'middleeast', 'russia'] };
var ural = {owner:'', unitCount:1, neighbours: ['russia', 'afghanistan', 'china', 'siberia'] };
var venezuela = {owner:'', unitCount:1, neighbours: ['centralamerica', 'brazil', 'peru'] };
var westerneurope = {owner:'', unitCount:1, neighbours: ['greatbritain', 'northerneurope', 'southerneurope', 'northafrica'] };
var westernunitedstates = {owner:'', unitCount:1, neighbours: ['alberta', 'ontario', 'easternunitedstates', 'centralamerica'] };
var yakutsk = {owner:'', unitCount:1, neighbours: ['siberia', 'easternrussia', 'kamchatka'] };
// END Country information		-		-		-		-		-		-		-		-		-		-		-		-		-		-		-