/* 
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
var selectedCountry;
var countryList = ["cn_china","cn_nz","cn_whatev","cn_lol"];
var countryArmies = [0,0,0,0];
var countryOwners = [0,0,0,0];
var countryColors = {};
   countryColors["cn_china"] = "blue";
   countryColors["cn_nz"] = "red";
   countryColors["cn_whatev"] = "green";
   countryColors["cn_lol"] = "purple";
var countryNames = {};
   countryNames["cn_china"] = "China";
   countryNames["cn_nz"] = "NZ";
   countryNames["cn_whatev"] = "Whateverland";
   countryNames["cn_lol"] = "LOLland";

function setCountryPosition(country, x, y)
{
   $(country).css({
       position: "absolute",
       top: y + "px",
       left: x + "px"
   });
}
function setSelectedCountry(event)
{
   console.log(event.data.n);
   //if (selectedCountry == n) return;
   $("#"+countryList[selectedCountry]).css("background",countryColors[countryList[selectedCountry]]);
   selectedCountry = event.data.n;
   $("#"+countryList[event.data.n]).css("background","white");    
}
function initCountries(){
   //Initialise some countries:
   for (var i = 0; i < countryList.length; i++)
   {
       $("#main").append("<div id=\""+countryList[i]+"\" class=\"country\">"+countryNames[countryList[i]]+"<p></p></div>");
   }
   for (var i = 0; i < countryList.length; i++)
   {
       $("#"+countryList[i]).css("background",countryColors[countryList[i]]);
   }
   for (var i = 0; i < countryList.length; i++)
   {
        $("#"+countryList[i]).click({n: i},setSelectedCountry);
   }

   $("div.country").width("100px");
   $("div.country").height("100px");
}
function updateDisplay()
{
console.log(countryList[selectedCountry],countryArmies[selectedCountry]);
$("#"+countryList[selectedCountry]+" p").html(countryArmies[selectedCountry]);
}
function increaseFunc()
{
countryArmies[selectedCountry] += 1;
updateDisplay()
}
function decreaseFunc()
{
countryArmies[selectedCountry] -= 1;
updateDisplay()
}
function initButtons()
{
$("#main").append("<div id=\"increase\" class=\"button\">increase</div>");
$("#main").append("<div id=\"decrease\" class=\"button\">decrease</div>");

$("div.button").css({
textAlign: "center",
background: "#BBBBBB",
height: "30px",
width: "100px",
       position: "absolute",
       right: "20px",
   });

$("#increase").css("top","20px");
$("#decrease").css("top","70px");

$("#increase").click(increaseFunc);
$("#decrease").click(decreaseFunc);
}

$(document).ready(function() {
   console.log('ready');
   initCountries();
initButtons();
   console.log(countryList);
   console.log(countryNames);
   console.log(countryColors);
   
   console.log('countries initialised');
   setCountryPosition("#cn_nz",50,100);
   setCountryPosition("#cn_china",200,50);
   setCountryPosition("#cn_whatev",50,200);
   setCountryPosition("#cn_lol",300,450);
});