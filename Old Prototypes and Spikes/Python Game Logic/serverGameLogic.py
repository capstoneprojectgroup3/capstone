__author__ = 'mkroll'

from riskyserv import *
import random
from random import shuffle


#the base implementation of list of countries
#warning this will need to be changed if map is changed
baseListOfCountries = [["alaska", "", 1], ["northwestterritory", "", 1], ["ontario", "", 1], ["quebec", "", 1], ["easternunitedstates", "", 1], ["centralamerica", "", 1], ["venezuela", "", 1], ["peru", "", 1], ["brazil", "", 1], ["argentina", "", 1], ["greenland", "", 1], ["iceland", "", 1], ["greatbritain", "", 1], ["westerneurope", "", 1], ["notherneurope", "", 1], ["southerneurope", "", 1], ["scandinavia", "", 1], ["kamchatka", "", 1], ["japan", "", 1], ["newguinea", "", 1], ["indonesia", "", 1], ["austrailia", "", 1], ["newzealand", "", 1], ["madagascar", "", 1], ["southafrica", "", 1], ["northafrica", "", 1], ["egypt", "", 1], ["eastafrica", "", 1], ["congo", "", 1], ["russia", "", 1], ["middleeast", "", 1], ["afghanastan", "", 1], ["ural", "", 1], ["yakutsk", "", 1], ["irkutsk", "", 1], ["mongolia", "", 1], ["siberia", "", 1], ["india", "", 1], ["china", "", 1], ["siam", "", 1], ["westernunitedstates", "", 1], ["alberta", "", 1], ]

#obtain the game state from the server to use in different functions
def getCurrentGameState(gameID):

    #dummy implementation of the gamestate, will have to be fetched from storage
    #gameState = {'greenland': ['marcel', 7], 'northwestterritory': ['marcel', 2], 'egypt': ['marcel', 1], 'india': ['marcel', 1]}
    state = MainLobby.getGameById(gameID)

    gameState = {'greenland': ['marcel', 7], 'northwestterritory': ['john', 2], 'egypt': ['john', 1], 'india': ['marcel', 1], 'congo': ['isaac', 1], 'quebec': ['john', 1], 'japan': ['isaac', 1], 'brazil': ['marcel', 1], 'madagascar': ['marcel', 1], 'ontario': ['marcel', 1], 'westernunitedstates': ['john', 1], 'middleeast': ['marcel', 1], 'eastafrica': ['sam', 1], 'southafrica': ['isaac', 1], 'yakutsk': ['marcel', 1], 'siberia': ['marcel', 1], 'peru': ['isaac', 1], 'newguinea': ['john', 1], 'afghanastan': ['marcel', 1], 'argentina': ['isaac', 1], 'venezuela': ['john', 1], 'iceland': ['isaac', 1], 'irkutsk': ['isaac', 1], 'westerneurope': ['john', 1], 'alberta': ['isaac', 1], 'centralamerica': ['sam', 1], 'china': ['john', 1], 'mongolia': ['sam', 1], 'southerneurope': ['sam', 1], 'ural': ['sam', 1], 'siam': ['isaac', 1], 'kamchatka': ['sam', 1], 'easternunitedstates': ['isaac', 1], 'scandinavia': ['john', 1], 'notherneurope': ['sam', 1], 'indonesia': ['marcel', 1], 'greatbritain': ['john', 1], 'alaska': ['john', 1], 'newzealand': ['sam', 1], 'northafrica': ['sam', 1], 'austrailia': ['sam', 1], 'russia': ['marcel', 1]}

    return state

#generate a randomized turn order from an initial input list of players
def generateTurnOrder(listOfPlayers):
    shuffle(listOfPlayers)
    return listOfPlayers

#retrieve the turn order as is stored on the server
def getTurnOrder():

    #dummy implementation at present, will pull actual one from the storage on the server
    turnOrder = ['sam', 'marcel', 'isaac', 'john']

    return turnOrder


def generateInitialGameState(listOfPlayers):
    #used to instansiate the initial game state when the game starts

    #makes a list of all available country indexes
    availableCountries = [x for x in range(len(baseListOfCountries))]

    #loop while there are countries available
    while len(availableCountries) >= 1:
        #loop through the list of players dividing countries equally
        for player in listOfPlayers:
            #if only one remains assign and exit loop
            if len(availableCountries) == 0:
                break
            #assign country to player then remove that reference from the available list
            ran = random.randint(0, len(availableCountries)-1)
            baseListOfCountries[availableCountries[ran]][1] = player
            del availableCountries[ran]

    #convert the output into a dictionary so that values can be obtained faster, as to avoid
    #iterating through each individual list
    storageDictionary = {}
    for items in baseListOfCountries:
        storageDictionary[items[0]] = [items[1], items[2]]

    #return the finishing init gamestate dictionary
    return storageDictionary

#small helper function to roll an amount of dice and return a list of rolls from highest to lowest
#varies on army size and whether or no the army is attacking or defending
def rolldice(armieSize, type):
    rollAmount = 0
    #type represents attack or defend
    if (armieSize == 1) and (type == "attack"):
        rollAmount = 1
    elif (armieSize == 2) and (type == "attack"):
        rollAmount = 2
    elif (armieSize > 2) and (type == "attack"):
        rollAmount = 3
    elif (armieSize == 1) and (type == "defend"):
        rollAmount = 1
    elif (armieSize == 2) and (type == "defend"):
        rollAmount = 2
    elif (armieSize > 2) and (type == "defend"):
        rollAmount = 2

    diceroll = []

    #roll the random dice rolls
    for i in range(rollAmount):
        diceroll.append(random.randint(1,6))

    #sort from highest to lowest
    diceroll.sort(reverse=True)

    return diceroll

def actionPerformed(inputList, gameID):

    #used to deal with multiple different actions take during the game
    #if statements to deal with each different input of data

    """this statement deals with if one country is attack one another returns the result of the combat"""
    if inputList[0] == "attack":
        #get current game state values
        gameState = getCurrentGameState(gameID)

        #determine the attack and defending amounts
        attackingAmount = (gameState[inputList[1]][1])-1
        defendingAmount = gameState[inputList[2]][1]

        #generate the combat dice lists for each side
        attackingRolls = rolldice(attackingAmount, "attack")
        defendingRolls = rolldice(defendingAmount, "defend")

        #keep track of losses on each side
        attackerLosses = 0
        defenderLosses = 0

        while len(attackingRolls) and len(defendingRolls):
            #depending on outcome increment loss totals
            if max(attackingRolls) > max(defendingRolls):
                defenderLosses += 1
            else:
                attackerLosses += 1

            #remove checked totals from lists and continue until battle is completed
            del attackingRolls[0]
            del defendingRolls[0]

        return [attackerLosses, defenderLosses]

    #this deals with when a player places an army
    #updates the game state accordingly and returns it to each user??
    elif inputList[0] == "placearmie":

        #parse the gamestate
        gameState = getCurrentGameState(gameID)

        #obtain the values within the list and update them with the placement armies
        giveUnits = gameState.get(inputList[1])
        giveUnits[1] = giveUnits[1] + inputList[2]
        gameState[inputList[1]] = giveUnits

        #return the new updated gamestate
        return gameState

    #this deals with end of turn reinforcement by a player
    #updates the game state accordingly and returns it to each user
    elif inputList[0] == "reinforce":
        #parse the gamestate
        gameState = getCurrentGameState(gameID)

        #obtain the values within the list and remove them from the associated country
        removeUnits = gameState.get(inputList[1])
        removeUnits[1] = removeUnits[1] - inputList[3]
        gameState[inputList[1]] = removeUnits

        #obtain the values within the list and add then to the associated country
        giveUnits = gameState.get(inputList[2])
        giveUnits[1] = giveUnits[1] + inputList[3]
        gameState[inputList[2]] = giveUnits

        #return the new updated gamestate
        return gameState

    #this deals with when a player ends their turn, simply returns the next active player to
    #all players, retrieves from session storage
    elif inputList[0] == "turnend":
        #obtain the turn order stored on the server
        turnOrder = getTurnOrder()

        #start looping through an enumeration of the turn order
        for i, item in enumerate(turnOrder):
            if item == inputList[1]:
                #if item is the last item in the list return the first item in the list
                if i == len(turnOrder)-1:
                    nextPlayer = turnOrder[0]
                    return nextPlayer
                #else return the current index plus one from the turn order list
                else:
                    nextPlayer = turnOrder[i+1]
                    return nextPlayer


#check to see if a player has won the game
def hasWon(playerName,gameID):
    #get gamestate
    gameState = getCurrentGameState(gameID)

    #create set
    setOfPlayers = set([])

    #add all owners to the set at max values in set is N players
    for items in gameState.values():
        setOfPlayers.add(items[0])

    #check if only one person is in the set, and if that person is the active player
    #if so return True indicating he has won and the game should end
    if (len(setOfPlayers) == 1) and (setOfPlayers.pop() == playerName):
        return True
    #if not return false, the game continues
    else:
        return False

"""testing commands to test all functions within this file"""
#print actionPerformed(["attack", "greenland", "northwestterritory"])
#print actionPerformed(["placearmie", "easternunitedstates", 10])
#print actionPerformed(["reinforce", "greenland", "austrailia", 6])
#print actionPerformed(["turnend", "marcel"])

#print initGameState(["marcel", "john", "isaac", "sam"])

#print rolldice(3,"attack")
#print rolldice(3,"defend")
#print hasWon("marcel")
#print generateTurnOrder(["marcel", "john", "isaac", "sam"])
