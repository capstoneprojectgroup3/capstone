'''
    riskyserv.py

    group 3 capstone 2014

    dependencies:
        The following libraries are required on the machine that runs this software
        python: twisted (twisted-matrix.com) which also requires zope interface(https://pypi.python.org/pypi/zope.interface#download)
                autobahn (autobahn.ws)
        to easily install these dependencies with pip:

            pip install zope.interface twisted autobahn
'''

import sys
import hashlib
import time
import random
from random import shuffle

from twisted.python import log
from twisted.internet import reactor
from autobahn.twisted.websocket import WebSocketServerProtocol, \
                                        WebSocketServerFactory


initialPieces = {2:40,3:35,4:30,5:25,6:20}
games = {}
clients = {}
sessionid=19756284
gameid = 64107

class lobby:
    '''Main game object that holds all players and games'''
    def __init__(self):
        self.players = []
        self.games = []
        self.clients=set()
        self.chatClients=set()

    def getGameById(self,gmid):
        '''returns a game object if there is one matching the givin gameid, otherwise None'''
        for g in self.games:
            print "checkig if %s = %s" % (str(g.gid),gmid)
            if str(g.gid) == str(gmid):
                return g
        print "game not found"
        return None

    def chatAll(self,sid,msg):
        '''Sends a chat mesage to all websock clients in the chatClients set'''
        #
        # todo
        # periodicaly prune chatcCients of defunct clients(ones that have refreshed their browser or disconnected uncleanly)
        # so that chatAll has no chance of error
        #
        global clientBySid,nameBySid
        print "sending chat message: %s" % msg
        for c in self.chatClients:
            c.sendMessage("chat,<%s>%s" % (nameBySid.get(sid),msg))

    def newPlayer(self,name,sid,ws):
        '''	Adds a new player object to the lobby that sets references to their name, id and websock client
            Also notifies connect clients that a ew player has joined the lobby'''
        self.players.append(player(name,str(sid),ws))
        #
        #todo:
        for p in self.clients:
            p.sendMessage("newuser,%s" % name)

        return '%s' % (str(sessionid))

    def remPlayer(self,sid):
        for p in self.players:
            if str(p.sid) == str(sid):
                del p
            break
        return "removed,%s" % str(sid)

    def getPlayerById(self,sid):
        for p in self.players:
            if str(p.sid) == str(sid):
                return p
        return "Error: Player not found"

    def getPlayers(self):
        tp=[]
        tp[:]=[]
        for p in self.players:
            tp.append(p.name)
        return(",".join(tp))

    def getsid(self,name):
        s=-1
        for p in self.players:
            if p.name == name:
                s=p.sid
                break
    # note: returns -1 if not found
        return str(s)

    def newGame(self,initiatingplayersid,gamename="",privacy=""):
        global gameid,clientsBySid
        g = game(gamename,privacy)
        self.games.append(g)
        g.players[initiatingplayersid]=[self.getPlayerById(initiatingplayersid).name,clientsBySid.get(initiatingplayersid)]
        g.clients.append(clientsBySid.get(initiatingplayersid))
        gameid += 1
        return g.gid

    def getGameStatus(self,gid):
        return str(self.getGameById(gid).status)

class game:
    def __init__(self,name="",privacy=False):
        global gameid
        self.clients = []
        self.players = dict()	# sid:[name,wsclient]
        self.turnOrder = []
        self.board = gamemap()
        self.gid = str(gameid)
        gameid +=1
        self.name = name
        self.password=privacy
        self.state = 0		# waiting for players, 1 = ingame, 2 game completed - additional states could be used for which player input is required from etc


    def start(self,gameID):
        gameStart = MainLobby.getGameById(gameID)
        gameStart.board.locations = generateInitialGameState(gameStart.players.keys(), gameID)

        gameStart.turnOrder = (generateTurnOrder(gameID))

        for client in self.clients:
            client.sendMessage("gameStart,%s" % str(self.gid))

class gamemap:
    def __init__(self):
        self.locations = [["alaska", "", 1], ["northwestterritory", "", 1], ["ontario", "", 1], ["quebec", "", 1], ["easternunitedstates", "", 1], ["centralamerica", "", 1], ["venezuela", "", 1], ["peru", "", 1], ["brazil", "", 1], ["argentina", "", 1], ["greenland", "", 1], ["iceland", "", 1], ["greatbritain", "", 1], ["westerneurope", "", 1], ["notherneurope", "", 1], ["southerneurope", "", 1], ["scandinavia", "", 1], ["kamchatka", "", 1], ["japan", "", 1], ["newguinea", "", 1], ["indonesia", "", 1], ["austrailia", "", 1], ["newzealand", "", 1], ["madagascar", "", 1], ["southafrica", "", 1], ["northafrica", "", 1], ["egypt", "", 1], ["eastafrica", "", 1], ["congo", "", 1], ["russia", "", 1], ["middleeast", "", 1], ["afghanastan", "", 1], ["ural", "", 1], ["yakutsk", "", 1], ["irkutsk", "", 1], ["mongolia", "", 1], ["siberia", "", 1], ["india", "", 1], ["china", "", 1], ["siam", "", 1], ["westernunitedstates", "", 1], ["alberta", "", 1], ]

class player:
    def __init__(self,name=None,sid=None,wsclient=None):
        self.name = name
        self.sid = sid
        self.reinforcements=0
        self.cards=[]
        self.ingame = False
        self.matchmking = False
        self.wsclient = wsclient
        self.lastseen = time.gmtime()
        self.initiator = False

    def addReinforcements(self,r):
        self.reinforcements +=r

    def addCard(self,c):
        self.cards.append(c)

class card:
    def __init__(self):
        self.type=None

class RiskyProtocol(WebSocketServerProtocol):
    global clientsBySid,nameBySid

    def onConnect(self, request):
        print("Client connecting: {0}".format(request.peer))
        MainLobby.clients.add(self)

    def onOpen(self):
        print("WebSocket connection open.")

    def onMessage(self, payload, isBinary):
        if isBinary:
            print("Binary message received: {0} bytes".format(len(payload)))
        else:
            print payload
            self.responder(payload)
    #self.responder(payload.decode('utf8'))
    #print("Text message received: {0}".format(payload.decode('utf8')))
    ## echo back message verbatim
    #self.sendMessage(payload, isBinary)

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))

    def responder(self,line):
        global clientsBySid,nameBySid		# hash indexes for client and name lookups
        line=line.split(';')
        if len(line)<4:				  # all requests are in 4 sections, see API documentation
            self.sendMessage('Invalid request')
            return
        if line[0] == 'newuser':
            hash_object = hashlib.md5(line[3].encode())
            sessionid = str(hash_object.hexdigest())

            if not clientsBySid.has_key(sessionid):
                sid=MainLobby.newPlayer(line[3],sessionid,self)		    # new player called line[3]
                self.sendMessage("newuser;%s" % sessionid)				# return new players session id to track requests
                clientsBySid[sessionid]=self
                nameBySid[sessionid]=line[3]
                MainLobby.chatClients.add(self)
            else:
                self.sendMessage("Error: Client exists, updated connection info")
                clientsBySid[sessionid]=self

        if line[0] == 'newgame':
            gid = MainLobby.newGame(line[1],line[3])
            print "new game called by %s gameid assigned: %s" % (nameBySid.get(line[1]),str(gid))
            MainLobby.getGameById(str(gid)).clients.append(self)
            #MainLobby.clients.remove(self)
            pl = MainLobby.getPlayerById(line[1])
            pl.matchmaking = True
            pl.initiator = True
            self.sendMessage("newgame;%s" % gid)
        if line[0] == 'newprivgame':			# passworded game
            gameid = MainLobby.newGame(line[1],line[2],line[3])
            MainLobby.getGameById(gameid).clients.append(self)
            MainLobby.getGameById(gameid).players[line[0]]=[nameBySid(line[0]),self]
            #MainLobby.clients.remove(self)
            pl = MainLobby.getPlayerById(line[0])
            pl.matchmaking = True
            pl.initiator = True
            self.sendMessage("ok,%d" % gameid)
        if line[0] == 'sendchat':		 #lobby chat
            MainLobby.chatAll(line[1],line[3])
            self.sendMessage("ok")
        if line[0] == 'joingame':
            if isinstance(MainLobby.getGameById(line[3]),game):
                MainLobby.getGameById(line[3]).clients.append(self)
                MainLobby.getGameById(line[3]).player.append(nameBySid.get(line[0]))
                MainLobby.chatClients.remove(self)
                MainLobby.getPlayerById(line[0]).matchmaking = True

        if line[0] == 'startgame':
            g = MainLobby.getGameById(line[2])
            p = MainLobby.getPlayerById(line[1])
            if p.initiator==True:
                g.status=1
                g.start(line[2])

                storedPlayers = g.players
                dictOfPlayers = {}

                for key in storedPlayers.keys():
                    dictOfPlayers[key] = storedPlayers[key][0]


                		        # do initial game setup routine to inform players of:
                                # initial countrys, initial player turn


                #generateTurnOrder(line[2])
                self.sendMessage("startgame;%s;%s" % (line[2],dictOfPlayers))

            else:
                self.sendMessage("Error: You are not the game owner")

        if line[0] == 'lobbylist':
            self.sendMessage("lobbylist;%s" % MainLobby.getPlayers())

        if line[0] == 'initgamestate':
            print 'initgamestate called from %s' % line[1]

            self.sendMessage("initgamestate;%s" % getCurrentGameState(line[2]))
            #
            #
            # todo initialgamestate code, also could be called from startgame function

        if line[0] == 'startingcards':
            print 'starting cards called'
            #
            # todo
            # maybe redundant code, could be implemented under start

        if line[0] == 'attack':
            #passed information in the form
            #attack;sessionid;gameid;attackingcountry,defendingcountry
            attacker = line[1]

            details = line[3]

            detailsList = details.split(",")

            attackfrom = detailsList[0]
            attackto = detailsList[1]

            print "Received attack by %s going from %s to %s" % (attacker,attackfrom,attackto)

            listToPass = []

            listToPass.append(line[0])
            listToPass.append(detailsList[0])
            listToPass.append(detailsList[1])

            print listToPass

            resultStorage = actionPerformed(listToPass, line[2])

            storeResult = []
            storeResult.append(attackfrom)
            storeResult.append(attackto)
            storeResult.append(resultStorage[0])
            storeResult.append(resultStorage[1])

            for client in MainLobby.getGameById(line[2]).clients:
                client.sendMessage("attack;" + str(storeResult))

        if line[0] == 'reinforce':
            '''reinforces a countries army from an adjacent country'''
            details = line[3].split(',')
            print "received reinforce by %s from %s to %s of amount %s" % (line[1],details[0],details[1],details[2])
            listToPass = []

            listToPass.append(line[0])
            listToPass.append(details[0])
            listToPass.append(details[1])
            listToPass.append(int(details[2]))

            actionPerformed(listToPass,line[2])

            for client in MainLobby.getGameById(line[2]).clients:
                client.sendMessage("reinforce;"+ str(details))


        if line[0] == "placearmie":
            '''adds armies to the specified country'''
            details = line[3].split(',')
            print "received army placement by %s on %s amount %s" % (line[1],details[0],details[1])
            listToPass = []

            listToPass.append(line[0])
            listToPass.append(details[0])
            listToPass.append(int(details[1]))

            actionPerformed(listToPass,line[2])

            for client in MainLobby.getGameById(line[2]).clients:
                client.sendMessage("placearmie;" + str(details))

        if line[0] == 'turnend':
            '''return the next user in the list'''

            nextplayer = actionPerformed(line[0], line[2])

            for client in MainLobby.getGameById(line[2]).clients:
                client.sendMessage("turnend;" + str(nextplayer))

        if line[0] == 'drawcard':
            '''returns a gamecard, used when a player has conquered a territory'''
            print "received card request from %s" % line[1]
            #
            # todo:
            # Implement this function providing we are still providing cards in the game

def generateInitialGameState(listOfPlayers,gameID):
    #used to instansiate the initial game state when the game starts
    initGame = MainLobby.getGameById(gameID)
    defaultList = initGame.board.locations

    #makes a list of all available country indexes
    availableCountries = [x for x in range(len(defaultList))]

    #loop while there are countries available
    while len(availableCountries) >= 1:
        #loop through the list of players dividing countries equally
        for player in listOfPlayers:
            #if only one remains assign and exit loop
            if len(availableCountries) == 0:
                break
            #assign country to player then remove that reference from the available list
            ran = random.randint(0, len(availableCountries)-1)
            defaultList[availableCountries[ran]][1] = player
            del availableCountries[ran]

    #convert the output into a dictionary so that values can be obtained faster, as to avoid
    #iterating through each individual list
    storageDictionary = {}
    for items in defaultList:
        storageDictionary[items[0]] = [items[1], items[2]]

    #return the finishing init gamestate dictionary
    return storageDictionary

def getCurrentGameState(gameID):
    currentState = MainLobby.getGameById(gameID)
    dictFromStorage = currentState.board.locations

    return dictFromStorage

#generate a randomized turn order from an initial input list of players
def generateTurnOrder(gameID):

    turnHolder = MainLobby.getGameById(gameID)
    players = turnHolder.players.keys()

    playerList = []

    for user in players:
        playerList.append(user)

    shuffle(playerList)
    turnHolder.turnOrder = playerList

#retrieve the turn order as is stored on the server
def getTurnOrder(gameID):
    turnHolder = MainLobby.getGameById(gameID)
    turnOrder = turnHolder.turnOrder

    return turnOrder

def rolldice(armieSize, type):
    rollAmount = 0
    #type represents attack or defend
    if (armieSize == 1) and (type == "attack"):
        rollAmount = 1
    elif (armieSize == 2) and (type == "attack"):
        rollAmount = 2
    elif (armieSize > 2) and (type == "attack"):
        rollAmount = 3
    elif (armieSize == 1) and (type == "defend"):
        rollAmount = 1
    elif (armieSize == 2) and (type == "defend"):
        rollAmount = 2
    elif (armieSize > 2) and (type == "defend"):
        rollAmount = 2

    diceroll = []

    #roll the random dice rolls
    for i in range(rollAmount):
        diceroll.append(random.randint(1,6))

    #sort from highest to lowest
    diceroll.sort(reverse=True)

    return diceroll

def actionPerformed(inputList, gameID):


    #used to deal with multiple different actions take during the game
    #if statements to deal with each different input of data

    """this statement deals with if one country is attack one another returns the result of the combat"""
    if inputList[0] == "attack":
        modifyState = MainLobby.getGameById(gameID).board

        #get current game state values
        curState = getCurrentGameState(gameID)

        #determine the attack and defending amounts
        attackingAmount = (curState[inputList[1]][1])-1
        defendingAmount = curState[inputList[2]][1]

        #generate the combat dice lists for each side
        attackingRolls = rolldice(attackingAmount, "attack")
        defendingRolls = rolldice(defendingAmount, "defend")

        #keep track of losses on each side
        attackerLosses = 0
        defenderLosses = 0

        while len(attackingRolls) and len(defendingRolls):
            #depending on outcome increment loss totals
            if max(attackingRolls) > max(defendingRolls):
                defenderLosses += 1
            else:
                attackerLosses += 1

            #remove checked totals from lists and continue until battle is completed
            del attackingRolls[0]
            del defendingRolls[0]

        curState[inputList[1]][1] = curState[inputList[1]][1] - attackerLosses
        curState[inputList[2]][1] = curState[inputList[2]][1] - defenderLosses

        modifyState.locations = curState

        return [attackerLosses, defenderLosses]

    #this deals with when a player places an army
    #updates the game state accordingly and returns it to each user??
    elif inputList[0] == "placearmie":
        modifyState = MainLobby.getGameById(gameID).board

        #parse the gamestate
        curState = getCurrentGameState(gameID)

        #obtain the values within the list and update them with the placement armies
        giveUnits = curState.get(inputList[1])
        giveUnits[1] = giveUnits[1] + inputList[2]
        curState[inputList[1]] = giveUnits

        #return the new updated gamestate
        modifyState.locations = curState


    #this deals with end of turn reinforcement by a player
    #updates the game state accordingly and returns it to each user
    elif inputList[0] == "reinforce":
        modifyState = MainLobby.getGameById(gameID).board

        #parse the gamestate
        curState = getCurrentGameState(gameID)

        #obtain the values within the list and remove them from the associated country
        removeUnits = curState.get(inputList[1])
        removeUnits[1] = removeUnits[1] - inputList[3]
        curState[inputList[1]] = removeUnits

        #obtain the values within the list and add then to the associated country
        giveUnits = curState.get(inputList[2])
        giveUnits[1] = giveUnits[1] + inputList[3]
        curState[inputList[2]] = giveUnits

        #return the new updated gamestate
        modifyState.locations = curState


    #this deals with when a player ends their turn, simply returns the next active player to
    #all players, retrieves from session storage
    elif inputList[0] == "turnend":
        #obtain the turn order stored on the server
        turnOrder = getTurnOrder(gameID)

        #start looping through an enumeration of the turn order
        for i, item in enumerate(turnOrder):
            if item == inputList[1]:
                #if item is the last item in the list return the first item in the list
                if i == len(turnOrder)-1:
                    nextPlayer = turnOrder[0]
                    return nextPlayer
                #else return the current index plus one from the turn order list
                else:
                    nextPlayer = turnOrder[i+1]
                    return nextPlayer


#check to see if a player has won the game
def hasWon(playerName,gameID):
    #get gamestate
    gameState = getCurrentGameState(gameID)

    #create set
    setOfPlayers = set([])

    #add all owners to the set at max values in set is N players
    for items in gameState.values():
        setOfPlayers.add(items[0])

    #check if only one person is in the set, and if that person is the active player
    #if so return True indicating he has won and the game should end
    if (len(setOfPlayers) == 1) and (setOfPlayers.pop() == playerName):
        return True
    #if not return false, the game continues
    else:
        return False

if __name__ == '__main__':
    log.startLogging(sys.stdout)
    MainLobby = lobby()
    clientsBySid = dict()
    nameBySid = dict()
    factory = WebSocketServerFactory("ws://localhost:8080", debug = False)
    factory.protocol = RiskyProtocol
    reactor.listenTCP(8080, factory)
    reactor.run()