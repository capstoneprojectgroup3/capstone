from autobahn.twisted.websocket import WebSocketServerProtocol, \
                                       WebSocketServerFactory
import hashlib

initialPieces = {2:40,3:35,4:30,5:25,6:20}
games = {}
clients = {}
sessionid=0
gameid = 0

class lobby:
	
	def __init__(self):
		self.players = []
		self.games = []
	
	def newPlayer(self,name):
		global sessionid
		pl = player(name,sessionid)
		sessionid+=1
		if len(self.players)>0:
			others=[p.name for p in self.players]
		else: others=['']
		self.players.append(pl)
		return '%d' % (sessionid-1)
	
	def remPlayer(self,sid):
		for p in self.players:
			if str(p.sid) == str(sid):
				del p
				break
		return "removed,%s" % str(sid)
	
	def getPlayerById(self,id):
		for p in self.players:
			if str(p.sid) == str(id):
				return p
		return "Error: Player not found"
	
	def getPlayers(self):
		tp=[]
		tp[:]=[]
		for p in self.players:
			tp.append(p.name)
		return(",".join(tp))
		
	def getsid(self,name):
		s=-1
		for p in players:
			if p.name == name:
				s=p.sid
				break
		# note: returns -1 if not found
		return str(s)
		
	def newGame(self,initiatingplayersid):
		global gameid
		g = game(gameid)
		gameid +=1
		for p in self.players:
			if str(p.sid) == initiatingplayersid:
				g.players.append(p)
		self.games.append(g)
		return g.gid
		
class game:
	def __init__(self,gid=0):
		self.players = {}
		self.board = map()
		self.gid = gid
		self.state = 0 		# waiting for players, 1 = ingame, 2 game completed - additional states could be used for which player input is required from etc 

class map:
	def __init__(self):
		self.locations=[]
		while len(self.loactions)<42:
			self.loactions.append([])

			
class player:
	def __init__(self,name=None,sid=None):
		self.name = name
		self.sid = sid
		self.reinforcements=0
		self.cards=[]
		self.ingame = False
		self.matchmking = False
		self.wsclient = None
		
	def addReinforcements(r):
		self.reinforcements +=r
	
	def addCard(c):
		self.cards.append(c)
		
class card:
	def __init__(self):
		self.type=None
		

									   
class MyServerProtocol(WebSocketServerProtocol):

	def onConnect(self, request):
		print("Client connecting: {0}".format(request.peer))

	def onOpen(self):
		print("WebSocket connection open.")

	def onMessage(self, payload, isBinary):
		if isBinary:
			print("Binary message received: {0} bytes".format(len(payload)))
		else:
			self.responder(payload)
			#self.responder(payload.decode('utf8'))
			#print("Text message received: {0}".format(payload.decode('utf8')))
			## echo back message verbatim
			#self.sendMessage(payload, isBinary)

	def onClose(self, wasClean, code, reason):
		print("WebSocket connection closed: {0}".format(reason))
		
	def responder(self,line):
		line=line.split(',')
		if len(line)<2:
			self.sendMessage('Invalid request')
			return
		if line[0] == 'newp':
			sid=MainLobby.newPlayer(line[1]) 	# new player called line[1]
			
			self.sendMessage(sid)				# return new players session id to track requests
		if line[0] == 'getp':
			plist = MainLobby.getPlayers()
			self.sendMessage(plist)
		if line[0] == 'newg':
			MainLobby.newgame(line[1])
			
			

if __name__ == '__main__':

   import sys

   from twisted.python import log
   from twisted.internet import reactor

   log.startLogging(sys.stdout)
   MainLobby = lobby()
   factory = WebSocketServerFactory("ws://localhost:8080", debug = False)
   factory.protocol = MyServerProtocol

   reactor.listenTCP(8080, factory)
   reactor.run()