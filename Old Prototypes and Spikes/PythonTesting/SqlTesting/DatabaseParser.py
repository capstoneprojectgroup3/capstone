__author__ = 'Marcel Kroll'

import MySQLdb

def connectToDatabase():
    dbConnection = MySQLdb.connect(host="localhost", user="root", passwd="capstone", db="riskstorage")
    return dbConnection

def checkIfUserExists(uniqueId):
    dbConnection = connectToDatabase()

    cursor = dbConnection.cursor()

    cursor.execute("SELECT * FROM registered_users WHERE UNIQUEID LIKE %s", uniqueId)

    dbConnection.close()

    if cursor.fetchall():
        return True
    else:
        return False


def createUser(name, uniqueId):
    dbConnection = connectToDatabase()

    cursor = dbConnection.cursor()

    if ((len(name)== 0) or (len(uniqueId)==0)):
        return "Invalid name or uniqueId entered"

    else:
        cursor.execute("INSERT INTO registered_users (ID, NAME, UNIQUEID, WINLOSS) VALUES (NULL, '%s', '%s', '0/0')" % (name, uniqueId))

        dbConnection.commit()
        dbConnection.close()

def parseWinLoss(uniqueID):

    dbConnection = connectToDatabase()

    cursor = dbConnection.cursor()

    cursor.execute("SELECT WINLOSS FROM registered_users WHERE UNIQUEID LIKE %s",uniqueID)

    dbConnection.close()

    for item in cursor.fetchall():
        return item[0]

def getName(uniqueID):

    dbConnection = connectToDatabase()

    cursor = dbConnection.cursor()

    cursor.execute("SELECT NAME FROM registered_users WHERE UNIQUEID LIKE %s",uniqueID)

    dbConnection.close()

    for item in cursor.fetchall():
        return item[0]





#print (createUser("Marcel Kroll","dsa34mk34rl2lakm24lk243laq"))
#print checkIfUserExists("dsa34mk34rl2lakm24lk243laq")
#print getName("435j34nhrjn3l453j45hnj45h")
