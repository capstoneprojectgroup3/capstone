// global variables
var gameStatus = 0; // flag to indicate if users can use map
var gameSetupPhase = 1; // flag to indicate whether game in place army or attack mode

var myId = 1; // STRING - my id (sessionid in server) 
var myGameName = '';
var myColor = ''; // my armies color
var gameId = 0; // INT - the id of this specific game
var gameName = ''; // the name of this specific game

var playersDict;
var listOfUsers = []; // list of the users in the game - stored as sessionId's

var myTurn = 0;
var firstTurn = 1;
var firstCall;
var country; // index of country
var firstCountry;

var colors = ["blue","red","green","orange","purple","yellow"];
var unitCount = 4;

// Game Set Up Functions----------------------------------------------------------------------------------------------

// This function takes the initial game state receieved from
// the server and updates the map. Change owner/color/unit count of each country
function setUpMap(state){
    var x = state.split(',');
    var i = 0;
    for(i=0;i<42;i++){           
        graph_owner[i] = x[i];
        var newColor = graph_getOwnerColor(x[i]);
        document.getElementById(countryList[i]).setAttribute("fill",newColor);
    }     
};

// END Game Set Up Functions------------------------------------------------------------------------------------------


// Game functions - activated onclick of map
// Also code which responds to button presses such as 'Start Game' when these need to check something
// -------------------------------------------------------------------------
function processStartGame() {
	if (!gameStatus){ //check to make sure you're not already in a game before starting
			startGame()
		} else {
			window.alert("Already in game");
		}
};
function processNewGame(name) {
	if (!gameStatus){ //check to make sure you're not already in a game before starting
			newGame(name)
		} else {
			window.alert("Already in game");
		}
};
function processNewUser(email,name) {
	if (myId === 1){ //check to make sure you're not already a user - Id is initialised as 1 above
			newUser(email,name)
		} else {
			window.alert("Already a user");
		}
};

function myFunc(caller){
	country = graph_getCountryIndex(caller.id);
        if(gameStatus){
            if(gameSetupPhase){
                if(myTurn){
                    if(graph_owner[country]==myId){        
                        alert(countryList[country]+" has an extra army");
                        unitCount = unitCount - 1; // decrease the amount of armies the player has to distribute
                        if(unitCount==0){
                            gameSetupPhase = 0;        
                            document.getElementById("gameStage").value = "ATTACK";
                            document.getElementById("gameStage").style.color = "#F30021";
                        }
                        placeArmy(caller.id);
                    }
                }
            } else if(myTurn){ // is it my turn?
                if(firstTurn){
                    firstChoice(caller); // the function below this                     
                } else {
                    secondChoice(caller); // the second following function
                }
            } 
        }
};

function selectCountry(caller)
{
	caller.setAttribute("fill", "grey");
}
function deselectCountry(caller)
{
	caller.setAttribute("fill", myColor);
}

function firstChoice(caller){
	// check if the cell is owned
	if(graph_owner[country]==myId){ 
            if(graph_armies[country]>1){
                //caller.setAttribute("fill", "grey"); // its my first choice so change cell to blue
                selectCountry(caller);
                firstTurn = 0; // its not my first turn anymore
                firstCountry = country; // remember my first cell choice
                firstCall = caller; //and the cell in the table it belongs to 
            }            
	}       
};

function secondChoice(caller){
	// check to make sure I havn't already selected this cell
	if(graph_owner[country]!=myId){
            if(graph_neighbours(country,firstCountry)){ // check whether second selected cell is neighbour
                deselectCountry(firstCall);//firstCall is caller from firstChoice, so it is the selected country
                firstTurn = 1;
                //this means attacking that country:
                //but to attack, I need at least two armies
                if(graph_armies[firstCountry] >= 2) attack(firstCall.id,caller.id);
            }
        } else {// else if I own the country (if(graph_owner[country]***==***myId)) but its not my first choice then check if the first choice is a neighbour
        // and if so then take one of his armies and call reinforce if greater than 1 else do nothing
            if(graph_neighbours(country,firstCountry)){
                var max = graph_armies[firstCountry] - 1;
                var n = window.prompt("How many armies would you like to move? (Max: " + max,"");
                    if (n) {//if we got an answer, then do the reinforce and end turn
                        reinforce(firstCall.id,caller.id,n); //the handler for the reinforce message updates the armies
                        deselectCountry(firstCall);
                        endTurn();//you can only reinforce once per turn as the last thing you do
                    }
            }
        }
};

//Library for representing a graph data structure with some helper methods
//MAP DATA:
var mapdata = [['middleeast', 'china', 'ural', 'russia', 'india'], ['northernterritory', 'alberta'], ['alaska', 'northernterritory', 'ontario', 'westernunitedstates'], ['peru', 'brazil'], ['newguinea', 'indonesia', 'newzealand'], ['venezuela', 'peru', 'argentina'], ['venezuela', 'westernunitedstates', 'easternunitedstates'], ['thailand', 'india', 'afghanistan', 'ural', 'siberia', 'mongolia'], ['northafrica', 'eastafrica', 'southafrica'], ['egypt', 'northafrica', 'southafrica', 'congo'], ['mongolia', 'yakutsk', 'kamchatka', 'siberia'], ['centralamerica', 'westernunitedstates', 'ontario', 'quebec'], ['middleeast', 'norhtafrica', 'eastafrica'], ['iceland', 'northerneurope', 'westerneurope'], ['quebec', 'iceland'], ['greenland', 'greatbritain', 'scandinavia'], ['thailand', 'china', 'afghanistan', 'middleeast'], ['australia', 'newguinea', 'thailand'], ['china', 'kamchatka'], ['yakutsk', 'easternrussia', 'mongolia', 'japan'], ['southafrica'], ['egypt', 'russia', 'afghanistan', 'india', 'southereurope'], ['china', 'siberia', 'easternrussia', 'kamchatka', 'japan'], ['indonesia', 'australia', 'thailand'], ['australia'], ['egypt', 'eastafrica', 'congo', 'westerneurope'], ['westerneurope', 'southerneurope', 'russia', 'scandinavia'], ['alberta', 'alaska', 'ontario'], ['northernterritory', 'alberta', 'westernunitedstates', 'easternunitedstates', 'quebec'], ['venezuela', 'brazil', 'argentina'], ['easternunitedstates', 'ontario', 'greenland'], ['middleeast', 'southerneurope', 'northerneurope', 'scandinavia', 'afghanistan', 'ural'], ['iceland', 'northerneurope', 'russia'], ['china', 'india'], ['yakutsk', 'ural', 'china', 'mongolia', 'easternrussia'], ['congo', 'eastafrica', 'madagascar'], ['westerneurope', 'northerneurope', 'middleeast', 'russia'], ['russia', 'afghanistan', 'china', 'siberia'], ['centralamerica', 'brazil', 'peru'], ['greatbritain', 'northerneurope', 'southerneurope', 'northafrica'], ['alberta', 'ontario', 'easternunitedstates', 'centralamerica'], ['siberia', 'easternrussia', 'kamchatka']]

function graph_generateGraph(data)
{
    for (var i = 0; i < data.length; i++) graph_main[graph_main.length] = (graph_convertList(mapdata[i]));
    console.log(graph_main);
};
function graph_convertList(a)
{
    var r = [];
    for (var i = 0; i < a.length; i++) r[r.length] = (graph_getCountryIndex(a[i]));
    return r;
};

// There is a list of countries: [Albania, Australia, China, x...]
// These indices become a key for the graph data
// after generateGraph() has been run,
// graph_main[i] is a list of the neighbours of country i

countryList = ['afghanistan', 'alaska', 'alberta', 'argentina', 'australia', 'brazil', 'centralamerica', 'china', 'congo', 'eastafrica', 'easternrussia', 'easternunitedstates', 'egypt', 'greatbritain', 'greenland', 'iceland', 'india', 'indonesia', 'japan', 'kamchatka', 'madagascar', 'middleeast', 'mongolia', 'newguinea', 'newzealand', 'northafrica', 'northerneurope', 'northernterritory', 'ontario', 'peru', 'quebec', 'russia', 'scandinavia', 'siberia', 'southafrica', 'southerneurope', 'thailand', 'ural', 'venezuela', 'westerneurope', 'westernunitedstates', 'yakutsk']

var graph_main = [];
var graph_owner = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var graph_armies = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];

function graph_getOwnerColor(ownerId){ // parameter is owners sessionId
    var colr = listOfUsers.indexOf(playersDict[ownerId]);
    return colors[colr];
}
function graph_contains(l,n)
{
    for (var i = 0; i < l.length; i++) if (l[i] == n) return true;
    return false;
};
function graph_getNeighbours(a) //returns list of a's neighbours from the graph
{
    return graph_main[a];
};
function graph_neighbours(a,b) //checks if the list of a's neighbours contains b
{
    return (graph_contains(graph_main[a],b));
};
function graph_getCountryIndex(id)
{
    for (var i = 0; i < countryList.length; i++) if (countryList[i] == id) return i;
    return -1;
};
function graph_ownerOf(id){ 
	return graph_owner[graph_getCountryIndex(id)];
};
function graph_armiesIn(id){
	return graph_armies[graph_getCountryIndex(id)];
};

graph_generateGraph(mapdata);
// END game functions - activated onclick of map ------------------------------


// Web Sockets Setup-----------------------------------------------------------
var socket = null;
var isopen = false;
var sessionid = 0;

window.onload = function() {

    //socket = new WebSocket("ws://199.15.251.155:8080");
    socket = new WebSocket("ws://127.0.0.1:8080");
    socket.binaryType = "arraybuffer";

    socket.onopen = function() {
      console.log("Connected!");
      isopen = true;
   };

    socket.onmessage = function(e) {
        if (typeof e.data == "string") {
            console.log("Text message received: " + e.data);
        } else {
            var arr = new Uint8Array(e.data);
            var hex = '';
            for (var i = 0; i < arr.length; i++) {
                hex += ('00' + arr[i].toString(16)).substr(-2);
            }
            console.log("Binary message received: " + hex);
        }
            handler(e.data);
    };

    socket.onclose = function(e) {
        console.log("Connection closed.");
        socket = null;
        isopen = false;
    };
};
// end Web Socket Setup--------------------------------------------------------

// Websockets game functions---------------------------------------------------
function handler(response){ // function to receive data from server 
    response = response.split(';');
    if(response[0]==="newuser"){ //newuser messages contain the addressee in [1]
        myId = response[1];
    } else if (response[0].startsWith("Error")) { //if for some reason we made an invalid request, display it!
        window.alert(response[0]);
	} else if(response[0]==="mygame"){
        gameId = parseInt(response[1]);
    } else if(response[0]==="joingame"){
        var nothing = 0; // This code does nothing. This will be implemented later as part of lobby
    } else if(response[0]==="startgame"){
        playersDict = JSON.parse(response[2]);
        listOfUsers = response[3].split(',');
        var cIndex = listOfUsers.indexOf(myGameName);
        myColor = colors[cIndex];
        document.getElementById("displayMyColour").value = myColor.toUpperCase();
        document.getElementById("displayMyColour").style.color = myColor;
        document.getElementById("displayMyName").style.color = myColor;
        getGameState(); // calls initgamestate
    } else if(response[0]==="initgamestate"){ // 
        setUpMap(response[1]); //- *** THIS FUNCTION NEEDS WORK ***
        if(response[2]==myId){
            myTurn = 1;
        }
        gameStatus = 1;
        currentPlayerName = playersDict[response[2]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[2]])];
        document.getElementById("gameStage").value = "Place Armies";
        document.getElementById("gameStage").style.color = "#FF7100";
    } else if(response[0]==="placeunit"){
        var cntryIndx = graph_getCountryIndex(response[1]);
        graph_armies[cntryIndx] += parseInt(response[2]);
        if(response[3]==myId){
            myTurn = 1;
        } else {
            myTurn = 0;
        }
        document.getElementById("displayCurrentPlayer").value = playersDict[response[3]].toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[3]])];
    } else if(response[0]==="turn"){
        if(response[1]==myId){
            myTurn = 1;
        } else {
            myTurn = 0;
        }
        currentPlayerName = playersDict[response[1]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[1]])];
    } else if(response[0]=="reinforce"){ //this may work
        var renfrce = response[1].split(',');
        var countryFrom = renfrce[0];
        var countryTo = renfrce[1];
        var unitQuantity = parseInt(renfrce[2]);
        graph_armies[graph_getCountryIndex(countryFrom)] -= unitQuantity;
        graph_armies[graph_getCountryIndex(countryTo)] += unitQuantity;
    } else if(response[0]==="attack"){ // do this one - then do reinforce - game fin
        var moveStats = response[1].split(',');
        var ownerAttackingCountry = graph_ownerOf(moveStats[0]);
        var ownerDefendingCountry = graph_ownerOf(moveStats[1]);
			        
        var iAtt = graph_getCountryIndex(moveStats[0]);  // attacker index
        var iDef = graph_getCountryIndex(moveStats[1]);  // index for defender
        //both countries lose some armies - moveStats[2] is attacker losses, [3] is defender losses        
        
        var attackersArmies = graph_armies[iAtt]-1;
//        graph_armies[iAtt] -= parseInt(moveStats[2]);
        graph_armies[iDef] -= parseInt(moveStats[3]);


        if (graph_armies[iDef] <= 0) {//If attacker won, the defender will have 0 or -1 armies left!
            graph_owner[iDef] = ownerAttackingCountry; // defenders country now owned by attacker
            graph_armies[iDef] = attackersArmies-parseInt(moveStats[2]); // units moved to new country
            graph_armies[iAtt] = 1; // attacking country now has 1 unit
            document.getElementById(moveStats[1]).setAttribute("fill",graph_getOwnerColor(graph_owner[iDef]));//and set the colours
            firstTurn = 1; //you can continue to attack now
        } else {
            graph_armies[iAtt] -= parseInt(moveStats[2]);
        }
    } else if(response[0]==="gamefinished"){
        if(response[2]==gameId){
            gameStatus = 0;
            if(response[3]==myId){
                alert("YOU are the victor !");
            } else {
                alert("Player "+response[3]+" has won the game :-(");
            }
        }
    } else { //if we haven't interpreted the message at all, tell us about it
            console.log("Client needs to handle this:" +response[0]+response[1]);
	}
};

//The returns in these functions are there for use in the unit tests
function newUser(emailAddy,name){
    document.getElementById("displayMyName").value = name.toUpperCase();
    myGameName = name;
    if(isopen){
        socket.send("newuser;None;"+emailAddy+";"+name);
		return 1;
    } else {
        console.log("Connection not open");
		return 0;
    }
};

function newGame(gameName){
    if(isopen){
        socket.send("newgame;"+myId+";None;"+gameName);
		return 1;
    } else {
        console.log("Connection not open");
		return 0;
    }
};

function joinGame(idGameToJoin){
    if(gameStatus==0){
        var gId = idGameToJoin.toString();
        gameId = gId;
        if(isopen){
            socket.send("joingame;"+myId+";None;"+idGameToJoin);
            return 1;
        } else {
            console.log("Connection not open");
            return 0;
        }  
    }
};

function startGame(){
    var gId = gameId.toString();
    if(isopen){
        socket.send("startgame;"+myId+";"+gId+";None");
		return 1;
    } else {
        console.log("Connection not open");
		return 0;
    }        
};

function getGameState(){ //initgamestate
    var gId = gameId.toString();
    if(isopen){
        socket.send("initgamestate;"+myId+";"+gId+";None");
		return 1;
    } else {
        console.log("Connection not open");
		return 0;
    }
};

function placeArmy(countryPlaced){
    var gId = gameId.toString();
    myTurn = 0;
    if(isopen){
        socket.send("placeunit;"+myId+";"+gId+";"+countryPlaced+",1");
		return 1;
    } else {
        console.log("Connection not open");
		return 0;
    }
};

function attack(countryA,countryB) { // function not complete
    var gId = gameId.toString();
    if (isopen) {
        console.log("attack;"+myId+";"+gId+";"+countryA+','+countryB);
        socket.send("attack;"+myId+";"+gId+";"+countryA+','+countryB);
	   return 1;
    } else {
       console.log("Connection not opened.")
	   return 0;
    }
 };
 
 function reinforce(originCountry,countryToReinforce,unitQuantity) {
    var gId = gameId.toString();
    var UQ = unitQuantity.toString();
	alert(UQ);
    if (isopen) {
       socket.send("reinforce;"+myId+";"+gId+";"+originCountry+','+countryToReinforce+','+UQ);
       console.log("reinforce;"+myId+";"+gId+";"+originCountry+','+countryToReinforce+','+UQ);
	   return 1;
    } else {
       console.log("Connection not opened.")
	   return 0;
    }
 };
 
 function endTurn() {
    if(gameSetupPhase){
        alert("Place your units");
        return;
    } else {
        myTurn = 0;
        var gId = gameId.toString();
        if (isopen) {
            socket.send("turnend;"+myId+";"+gId+";None");
            return 1;
        } else {
            console.log("Connection not opened.");
            return 0;
        }
    }
 };

 
// end Websockets functions----------------------------------------------------
// begin Miscellaneous helpers and mods ---------------------------------------

if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.slice(0, str.length) == str;
  };
}

//end Miscellaneous helpers and mods
