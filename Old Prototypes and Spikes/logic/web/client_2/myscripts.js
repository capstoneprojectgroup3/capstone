// My information
var me = {myId:0 ,myGameName:'', myColor:''};
// END My information

// Global variables
var gameStarted = 0; // flag to indicate if users can use map
var gameSetupPhase = 1; // flag to indicate whether game in place army or attack mode

var gameId = 0; // INT - the id of this specific game
var gameName = ''; // STRING - the name of this specific game
var playersDict = {}; // OBJECT - {id:name}
var listOfUsers = []; // ARRAY - list of the users in the game - stored as sessionId's
var session; // A variable for connection to server
var itsMyGame = 0; // Flag to indicate whether I made the game

var myTurn = 0; // Flag to indicate if it is my turn
var firstTurn = 1; // Flag to indicate whether this is my first country choice in this turn
var firstCountry; // The caller object of the first country I chose in this turn

var colors = ["blue","red","green","orange","purple","yellow"];
var unitCount = 40;
// initialPieces = {2:40,3:35,4:30,5:25,6:20}
//END Global variables


// Functions activated by selecting map	-	-	-	-	-	-	-	-	-	-	-	-	-
function myFunc(caller){ // when you click on the map this function is called
	if(gameStarted){
		if(gameSetupPhase){
			if(myTurn&&window[caller.id].owner==me.myId){
				unitCount -=1;
				if(unitCount==0){
					gameSetupPhase = 0; 
					document.getElementById("deployDiv").style.visibility="hidden";					
					document.getElementById("gameStage").value = "ATTACK";
					document.getElementById("gameStage").style.color = "#F30021";
				}
				document.getElementById("deployMeter").value = unitCount;
				myTurn = 0;
				placeUnit(caller.id);
			}
		} else if(myTurn){ // its my turn and I can attack or reinforce
			if(firstTurn){
				firstChoice(caller);
			} else {
				console.log(window[firstCountry.id].neighbours.indexOf(caller.id),"index");
				console.log(window[firstCountry.id].neighbours,"neighbours");
				if(caller.id==firstCountry.id){
					deselectCountry(firstCountry);
					firstTurn = 1;
				} else if(window[firstCountry.id].neighbours.indexOf(caller.id)>=0){
					secondChoice(caller);
				}
			}
		}
	}
	return;
};

function firstChoice(caller){
	if(window[caller.id].owner==me.myId){ 
		if(window[caller.id].unitCount>1){
			selectCountry(caller);
			firstTurn = 0; // its not my first turn anymore
			firstCountry = caller; // remember my first cell choice 
		}            
	} 
	return;
};

function secondChoice(caller){
	// check to make sure I havn't already selected this country
	if(window[caller.id].owner!=me.myId){ // if true then I must be attacking
		console.log("im trying to attack");
		if(window[firstCountry.id].neighbours.indexOf(caller.id)>=0){// check whether second selected country is neighbour
			console.log("Second country is a neighbour");
			if(window[firstCountry.id].unitCount >=2){//but to attack, I need at least two armies
				deselectCountry(firstCountry); // change the first selected country back to my color	
				firstTurn = 1;					
				attack(firstCountry,caller);
			}
		}
	} else { // I must be reinforcing
		if(window[firstCountry.id].neighbours.indexOf(caller.id)>=0){// check whether second selected country is neighbour
			console.log("The country I want to reinforce is a neighbour");
			selectCountry(caller);
			var max = window[firstCountry.id].unitCount - 1;
			var n = -1;
			while(n<0){
				n = window.prompt("How many armies would you like to move? (Max: " + max,"");
				if(n>max){
					n = -1;
				}
			}
			if(n==0){
				deselectCountry(caller);
				deselectCountry(firstCountry);
				firstTurn = 1;
			} else {
				deselectCountry(caller);
				deselectCountry(firstCountry);
				console.log("calling reinforce");
				reinforce(firstCountry,caller,n);
				firstTurn = 1;
			}	
		}
	}
	return;
};
function selectCountry(caller)
{
	caller.setAttribute("fill", "grey");
	return;
};
function deselectCountry(caller)
{
	caller.setAttribute("fill", me.myColor);
	return;
};

// END Functions activated by selecting map	-	-	-	-	-	-	-	-	-	-	-	-	-

// Receive from server	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-			
function gamehandler(response){ // function to receive data from server (response is an array)
	console.log("Recieved info from server");
	console.log(response);
    if(response[0][0]=="startgame"){
        try{
            closeLobby();
            showGame();
        }
        catch(err){
            console.log("Function does not exist, probably testing version of client");
        }
		var i = 0;
		for(i=0;i<response[0][1].length;i++){
			playersDict[response[0][1][i][0]]= response[0][1][i][1];
			listOfUsers.push(response[0][1][i][1]);
		}
        var cIndex = listOfUsers.indexOf(me.myGameName);
        me.myColor = colors[cIndex];
        document.getElementById("displayMyColour").value = me.myColor.toUpperCase();
        document.getElementById("displayMyColour").style.color = me.myColor;
        document.getElementById("displayMyName").style.color = me.myColor;
       
        try{
	document.getElementById("startGameButton").style.visibility="hidden";
        }
        catch(err){
            console.log("Unable to hide button most likely due to testing version of client");
        }
        
		if(itsMyGame){
			getGameState(); // calls initgamestate
		}
    } else if(response[0]==="initgamestate"){ 
        setUpMap(response[1]); // line 298
        if(response[2]==me.myId){
            myTurn = 1;
			console.log("MY TURN");
        }
        gameStarted = 1;
        currentPlayerName = playersDict[response[2]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[2]])];
        document.getElementById("gameStage").value = "Place Armies";
        document.getElementById("gameStage").style.color = "#FF7100";
    } else if(response[0]==="placeunit"){
        var country = response[1];
		window[country].unitCount += 1;
        if(response[2]==me.myId){
            myTurn = 1;
        } else {
            myTurn = 0;
        }
        document.getElementById("displayCurrentPlayer").value = playersDict[response[2]].toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[2]])];
    } else if(response[0]==="turn"){
        if(response[1]==me.myId){
            myTurn = 1;
        } else {
            myTurn = 0;
        }
        currentPlayerName = playersDict[response[1]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[1]])];
    } else if(response[0]==="reinforce"){
		console.log(response);
		console.log("The above is received from server on reinforce");
        var countryFrom = response[1];
        var countryTo = response[2];
        var unitQuantity = parseInt(response[3]);
        window[countryFrom].unitCount -= unitQuantity;
		console.log(countryFrom);
		console.log(countryTo);
        window[countryTo].unitCount += unitQuantity;
    } else if(response[0]==="attack"){
        var attackingCountry = response[1];
        var defendingCountry = response[2];
		var attackerLoss = parseInt(response[3]);
		var defenderLoss = parseInt(response[4]);

        if (window[defendingCountry].unitCount-defenderLoss <= 0) {//If attacker won, the defender will have 0 or -1 armies left!
            window[defendingCountry].owner = window[attackingCountry].owner;// defenders country now owned by attacker
            window[defendingCountry].unitCount = window[attackingCountry].unitCount-1+attackerLoss; // units moved to new country
            window[attackingCountry].unitCount = 1; // attacking country now has 1 unit
            document.getElementById(defendingCountry).setAttribute("fill",colors[listOfUsers.indexOf(playersDict[window[attackingCountry].owner])]);//and set the colours
            firstTurn = 1; //you can continue to attack now
        } else {
            window[defendingCountry].unitCount -= defenderLoss;
			window[attackingCountry].unitCount -= attackerLoss;
        }
    } else if(response[0]==="gamefinished"){
        if(response[2]==gameId){
            gameStatus = 0;
            if(response[3]==me.myId){
                alert("YOU are the victor !");
            } else {
                alert("Player "+response[3]+" has won the game :-(");
            }
        }
    } else { //if we haven't interpreted the message at all, tell us about it
            console.log("Client needs to handle this:" +response[0]);
	}
	return;
};
// END Receive from server	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-			


// Send to server functions	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
function getLobbyList(){
    session.call('com.example.lobbylist', [me.myId]).then(
                           function(res){
                               var amountOfUsers = res[0].length;
                               for (var i=0; i<amountOfUsers; i++){
                                   try{
                                   document.getElementById("playerbox").innerHTML = document.getElementById("playerbox").innerHTML + "\n" + res[0][i];
                                    }
                                    catch(err){
                                        console.log("Unable to change HTML element, due to usage of test client")
                                    }
                                   
                        }
         
                    }                           
                );
}

function newUser(name,emailAddy){
    document.getElementById("displayMyName").value = name.toUpperCase();
    me.myGameName = name;
	session.call('com.example.newuser', [name,emailAddy]).then( // Send info to the server and wait for the response
	function (res) {
		me.myId = res[0];
		console.log("newUser response:", res);
                try{
		document.getElementById("newUserButton").style.visibility="hidden";
                }
                catch(err){
                    console.log("Unable to hide button most likely due to testing version of client");
                }
                
                getLobbyList();
	})
	return;
};

function newGame(gName){
	gameName = gName;
	session.call('com.example.newgame', [me.myId,gameName]).then( // Send info to the server and wait for the response
	function (res) {
		gameId = res[0];
		console.log("newGame response:", res);
		mount='com.example.'+gameId;
		session.subscribe(mount,gamehandler); // Make it so I receive all server msgs that have my game id
		console.log("subscribed to game channel: " + mount);
		document.getElementById("newGameButton").style.visibility="hidden";	
		document.getElementById("joinGameButton").style.visibility="hidden";	
	})
	return;
};

function joinGame(gameToJoin){
    if(gameStarted==0){
		session.call('com.example.joingame', [me.myId,gameToJoin]).then(
		function (res) {
			gameId = res[0];
			gameName = res[1];
			console.log("joinGame response:", res);
			mount='com.example.'+gameId;
			session.subscribe(mount,gamehandler) // Make it so I receive all server msgs that have my game id
			console.log("subscribed to game channel: " + mount)	
			document.getElementById("newGameButton").style.visibility="hidden";	
			document.getElementById("joinGameButton").style.visibility="hidden";
			document.getElementById("startGameButton").style.visibility="hidden";								
		})
	};
	return;
};

function startGame(){
	itsMyGame = 1;
	session.call('com.example.startgame', [me.myId,gameId]);
	return;
};

function getGameState(){ //initgamestate
	console.log("calling init");
	session.call('com.example.initgamestate', [me.myId,gameId]);
	return;
};

function placeUnit(country){ //Fortify
	console.log("Placing a unit");
	session.call('com.example.placeunit', [me.myId,gameId,country]);
	return;
};

function attack(countryA,countryB){
	console.log("Attack sent to server");
	console.log(me.myId,gameId,countryA.id,countryB.id);
	session.call('com.example.attack', [me.myId,gameId,countryA.id,countryB.id]);
	return;
};

function reinforce(originCountry,countryToReinforce,unitQuantity){
	session.call('com.example.reinforce', [me.myId,gameId,originCountry.id,countryToReinforce.id,unitQuantity]);
	return;
};

function endTurn(){
	if(gameSetupPhase){
		alert("Place your units");
        return;
	} else {
		myTurn = 0;
		console.log("Ending turn");
		session.call('com.example.turnend', [me.myId,gameId]);
	}
	return;
};

// END Send to server functions	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-

// Connection Setup----------------------------------------------------------------------
var session;
window.onload = function() {
	// Make a new connection to the server
	var connection = new autobahn.Connection({
	url: "ws://127.0.0.1:8080",
	realm: 'realm1'
	});
	
	connection.onopen = function (ses) {
	session = ses; // When the connection is established remember the session I have with the server
	console.log("connected");
	};
	
	connection.open(); // Keep the connection open
};
// END Connection Setup--------------------------------------------------------

function setUpMap(state){ // receive players map distribution from server and update country info
    var i = 0;
    for(i=0;i<42;i++){   
        window[countryList[i]].owner = state[i];
		if(state[i]==me.myId){
			unitCount--;
			console.log(unitCount,"unitCount");
		}
        var newColor = colors[listOfUsers.indexOf(playersDict[state[i]])];
        document.getElementById(countryList[i]).setAttribute("fill",newColor);
    } 
	return;    
};

function resumeGame(currentState){
	gameStarted = 0;
	gameSetupPhase = 0;
	unitCount = 0;
	/*
	gameId
	gameName
	playersDict
	listOfUsers
	*/
	return;
};

// Country information		-		-		-		-		-		-		-		-		-		-		-		-		-		-		-		
countryList = ['afghanistan', 'alaska', 'alberta', 'argentina', 'australia', 'brazil', 'centralamerica', 'china', 'congo', 'eastafrica', 'easternrussia', 'easternunitedstates', 'egypt', 'greatbritain', 'greenland', 'iceland', 'india', 'indonesia', 'japan', 'kamchatka', 'madagascar', 'middleeast', 'mongolia', 'newguinea', 'newzealand', 'northafrica', 'northerneurope', 'northernterritory', 'ontario', 'peru', 'quebec', 'russia', 'scandinavia', 'siberia', 'southafrica', 'southerneurope', 'thailand', 'ural', 'venezuela', 'westerneurope', 'westernunitedstates', 'yakutsk'];
var afghanistan = {owner:'', unitCount:1, neighbours: ['middleeast', 'china', 'ural', 'russia', 'india'] };
var alaska = {owner:'', unitCount:1, neighbours: ['northernterritory', 'alberta'] };
var alberta = {owner:'', unitCount:1, neighbours: ['alaska', 'northernterritory', 'ontario', 'westernunitedstates'] };
var argentina = {owner:'', unitCount:1, neighbours: ['peru', 'brazil'] };
var australia = {owner:'', unitCount:1, neighbours: ['newguinea', 'indonesia', 'newzealand'] };
var brazil = {owner:'', unitCount:1, neighbours: ['venezuela', 'peru', 'argentina'] };
var centralamerica = {owner:'', unitCount:1, neighbours: ['venezuela', 'westernunitedstates', 'easternunitedstates'] };
var china = {owner:'', unitCount:1, neighbours: ['thailand', 'india', 'afghanistan', 'ural', 'siberia', 'mongolia', 'japan'] };
var congo = {owner:'', unitCount:1, neighbours: ['northafrica', 'eastafrica', 'southafrica'] };
var eastafrica = {owner:'', unitCount:1, neighbours: ['egypt', 'northafrica', 'southafrica', 'congo'] };
var easternrussia = {owner:'', unitCount:1, neighbours: ['mongolia', 'yakutsk', 'kamchatka', 'siberia'] };
var easternunitedstates = {owner:'', unitCount:1, neighbours: ['centralamerica', 'westernunitedstates', 'ontario', 'quebec'] };
var egypt = {owner:'', unitCount:1, neighbours: ['middleeast', 'norhtafrica', 'eastafrica'] };
var greatbritain = {owner:'', unitCount:1, neighbours: ['iceland', 'northerneurope', 'westerneurope'] };
var greenland = {owner:'', unitCount:1, neighbours: ['quebec', 'iceland'] };
var iceland = {owner:'', unitCount:1, neighbours: ['greenland', 'greatbritain', 'scandinavia'] };
var india = {owner:'', unitCount:1, neighbours: ['thailand', 'china', 'afghanastan', 'middleeast'] };
var indonesia = {owner:'', unitCount:1, neighbours: ['australia', 'newguinea', 'thailand'] };
var japan = {owner:'', unitCount:1, neighbours: ['china', 'kamchatka'] };
var kamchatka = {owner:'', unitCount:1, neighbours: ['yakutsk', 'easternrussia', 'mongolia', 'japan'] };
var madagascar = {owner:'', unitCount:1, neighbours: ['southafrica'] };
var middleeast = {owner:'', unitCount:1, neighbours: ['egypt', 'russia', 'afghanistan', 'india', 'southereurope'] };
var mongolia = {owner:'', unitCount:1, neighbours: ['china', 'siberia', 'easternrussia', 'kamchatka', 'japan'] };
var newguinea = {owner:'', unitCount:1, neighbours: ['indonesia', 'australia', 'thailand'] };
var newzealand = {owner:'', unitCount:1, neighbours: ['australia'] };
var northafrica = {owner:'', unitCount:1, neighbours: ['egypt', 'eastafrica', 'congo', 'westerneurope'] };
var northerneurope = {owner:'', unitCount:1, neighbours: ['westerneurope', 'southerneurope', 'russia', 'scandinavia'] };
var northernterritory = {owner:'', unitCount:1, neighbours: ['alberta', 'alaska', 'ontario'] };
var ontario = {owner:'', unitCount:1, neighbours: ['northernterritory', 'alberta', 'westernunitedstates', 'easternunitedstates', 'quebec'] };
var peru = {owner:'', unitCount:1, neighbours: ['venezuela', 'brazil', 'argentina'] };
var quebec = {owner:'', unitCount:1, neighbours: ['easternunitedstates', 'ontario', 'greenland'] };
var russia = {owner:'', unitCount:1, neighbours: ['middleeast', 'southerneurope', 'northerneurope', 'scandinavia', 'afghanistan', 'ural'] };
var scandinavia = {owner:'', unitCount:1, neighbours: ['iceland', 'northerneurope', 'russia'] };
var thailand = {owner:'', unitCount:1, neighbours: ['china', 'india','indonesia','newguinea'] };
var siberia = {owner:'', unitCount:1, neighbours: ['yakutsk', 'ural', 'china', 'mongolia', 'easternrussia'] };
var southafrica = {owner:'', unitCount:1, neighbours: ['congo', 'eastafrica', 'madagascar'] };
var southerneurope = {owner:'', unitCount:1, neighbours: ['westerneurope', 'northerneurope', 'middleeast', 'russia'] };
var ural = {owner:'', unitCount:1, neighbours: ['russia', 'afghanistan', 'china', 'siberia'] };
var venezuela = {owner:'', unitCount:1, neighbours: ['centralamerica', 'brazil', 'peru'] };
var westerneurope = {owner:'', unitCount:1, neighbours: ['greatbritain', 'northerneurope', 'southerneurope', 'northafrica'] };
var westernunitedstates = {owner:'', unitCount:1, neighbours: ['alberta', 'ontario', 'easternunitedstates', 'centralamerica'] };
var yakutsk = {owner:'', unitCount:1, neighbours: ['siberia', 'easternrussia', 'kamchatka'] };
// END Country information		-		-		-		-		-		-		-		-		-		-		-		-		-		-		-