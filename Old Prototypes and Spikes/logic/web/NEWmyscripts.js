// My information
var me = {myId:0 ,myGameName:'', myColor:''};
// END My information

// Global variables
var gameStarted = 0; // flag to indicate if users can use map
var gameSetupPhase = 1; // flag to indicate whether game in place army or attack mode

var gameId = 0; // INT - the id of this specific game
var gameName = ''; // the name of this specific game
var playersDict = [];
var listOfUsers = []; // list of the users in the game - stored as sessionId's

var myTurn = 0;
var firstTurn = 1;
var firstCountry;

var colors = ["blue","red","green","orange","purple","yellow"];
var unitCount = 4;
//END Global variables


// Websockets game functions---------------------------------------------------------

function gamehandler(response){ // function to receive data from server 

     if(response[0]==="join"){
        var nothing = 0; // This code does nothing. This will be implemented later as part of lobby
		console.log('join event recieved');
    } else if(response[0][0]==="startgame"){
        playersDict = response[0][1]
		console.log('player: ' + playersDict);
        listOfUsers = [];
		for (p=0;p<playersDict.length;p++) {
			if (p%2==0) {
				listOfUsers.add(playersDict[p]);
			}
		}
        var cIndex = listOfUsers.indexOf(me.myGameName);
        me.myColor = colors[cIndex];
        document.getElementById("displayMyColour").value = me.myColor.toUpperCase();
        document.getElementById("displayMyColour").style.color = me.myColor;
        document.getElementById("displayMyName").style.color = me.myColor;
		document.getElementById("startGameButton").style.visibility="hidden";
        getGameState(); // calls initgamestate
    } else if(response[0]==="initgamestate"){ // 
        setUpMap(response[1]);
        if(response[2]==me.myId){
            myTurn = 1;
        }
        gameStarted = 1;
        currentPlayerName = playersDict[response[2]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[2]])];
        document.getElementById("gameStage").value = "Place Armies";
        document.getElementById("gameStage").style.color = "#FF7100";
    } else if(response[0]==="placeunit"){
        var country = response[1];
        var unitQ = parseInt(response[2]);
		window[country].unitCount += unitQ;
        if(response[3]==me.myId){
            myTurn = 1;
        } else {
            myTurn = 0;
        }
        document.getElementById("displayCurrentPlayer").value = playersDict[response[3]].toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[3]])];
    } else if(response[0]==="turn"){
        if(response[1]==me.myId){
            myTurn = 1;
        } else {
            myTurn = 0;
        }
        currentPlayerName = playersDict[response[1]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[1]])];
    } else if(response[0]=="reinforce"){
		console.log(response);
		console.log("The above is received from server on reinforce");
        var countryFrom = response[1];
        var countryTo = response[2];
        var unitQuantity = parseInt(response[3]);
        window[countryFrom].unitCount -= unitQuantity;
		console.log(countryFrom);
		console.log(countryTo);
        window[countryTo].unitCount += unitQuantity;
    } else if(response[0]==="attack"){
        var moveStats = response[1].split(',');
        var attackingCountry = moveStats[0];
        var defendingCountry = moveStats[1];
		var attackerLoss = parseInt(moveStats[2]);
		var defenderLoss = parseInt(moveStats[3]);

        if (window[defendingCountry].unitCount-defenderLoss <= 0) {//If attacker won, the defender will have 0 or -1 armies left!
            window[defendingCountry].owner = window[attackingCountry].owner;// defenders country now owned by attacker
            window[defendingCountry].unitCount = window[attackingCountry].unitCount-1+parseInt(moveStats[2]); // units moved to new country
            window[attackingCountry].unitCount = 1; // attacking country now has 1 unit
            document.getElementById(moveStats[1]).setAttribute("fill",colors[listOfUsers.indexOf(playersDict[window[attackingCountry].owner])]);//and set the colours
            firstTurn = 1; //you can continue to attack now
        } else {
            window[defendingCountry].unitCount -= defenderLoss;
			window[attackingCountry].unitCount -= attackerLoss;
        }
    } else if(response[0]==="gamefiished"){// < -- PURPOSELY DISABLED FOR NOW
        if(response[2]==gameId){
            gameStatus = 0;
            if(response[3]==me.myId){
                alert("YOU are the victor !");
            } else {
                alert("Player "+response[3]+" has won the game :-(");
            }
        }
    } else { //if we haven't interpreted the message at all, tell us about it
            console.log("Client needs to handle this:" +response);
	}
};

//The returns in these functions are there for use in the unit tests
function newUser(name,emailAddy){
    document.getElementById("displayMyName").value = name.toUpperCase();
    me.myGameName = name;
	session.call('com.example.newuser', [name,emailAddy]).then(
	   function (res) {
			me.myId = res[0];
			console.log("newuser response:", res);
			document.getElementById("newUserButton").style.visibility="hidden";	
			lobbylist();
	   })
	};
function newGame(gameName){

   session.call('com.example.newgame', [me.myId,gameName]).then(
				   function (res) {
						gameId = res[0];
						console.log("newgame response:", res);
						mount='com.example.'+gameId;
						session.subscribe(mount,gamehandler)
						console.log("subscribed to game channel: " + mount)	
						document.getElementById("newGameButton").style.visibility="hidden";	
						$("#joinGameButton").hide();						
				   })
				};

				
				
function joinGame(gname){
    if(gameStarted==0){
        session.call('com.example.joingame', [me.myId,gname]).then(
				   function (res) {
						gameId = res[0];
						gameName = res[1];
						console.log("joingame response:", res);
						mount='com.example.'+gameId;
						session.subscribe(mount,gamehandler)
						console.log("subscribed to game channel: " + mount)	
						$("#newGameButton").hide();	
						$("#joinGameButton").hide();		
						$("#startGameButton").hide();								
				   })
				};
};
function startGame(){
    var gId = gameId.toString();
	session.call('com.example.startgame', [me.myId,gId]);
};

function getGameState(){ //initgamestate
    var gId = gameId.toString();
	session.call('com.example.initgamestate', [me.myId,gId]);
	
};

function placeUnit(countryPlaced){
    var gId = gameId.toString();
    myTurn = 0;
    if(isopen){
        socket.send("placeunit;"+me.myId+";"+gId+";"+countryPlaced+",1");
		return 1;
    } else {
        console.log("Connection not open");
		return 0;
    }
};

function attack(countryA,countryB) {
    var gId = gameId.toString();
    if (isopen) {
        console.log("attack;"+me.myId+";"+gId+";"+countryA.id+','+countryB.id);
        socket.send("attack;"+me.myId+";"+gId+";"+countryA.id+','+countryB.id);
	   return 1;
    } else {
       console.log("Connection not opened.")
	   return 0;
    }
 };
 
 function reinforce(originCountry,countryToReinforce,unitQuantity) {
    var gId = gameId.toString();
    var UQ = unitQuantity.toString();
	console.log(UQ);
    if (isopen) {
       socket.send("reinforce;"+me.myId+";"+gId+";"+originCountry.id+','+countryToReinforce.id+','+UQ);
       console.log("reinforce;"+me.myId+";"+gId+";"+originCountry.id+','+countryToReinforce.id+','+UQ);
	   return 1;
    } else {
       console.log("Connection not opened.")
	   return 0;
    }
 };
 
 function endTurn() {
    if(gameSetupPhase){
        alert("Place your units");
        return;
    } else {
        myTurn = 0;
        var gId = gameId.toString();
        if (isopen) {
            socket.send("turnend;"+me.myId+";"+gId+";None");
            return 1;
        } else {
            console.log("Connection not opened.");
            return 0;
        }
    }
 };
// END Send to server functions	-	-	-	-	-	-	-	-	-	-	-	-	-
window.onload = function() {
         var connection = new autobahn.Connection({
            url: "ws://127.0.0.1:8080",
            realm: 'realm1'
         });
			

         connection.onopen = function (ses) {
			session = ses;
            console.log("connected");
			
			function onchat(msg) {
				console.log("CHAT: " + msg);
				$('#chat').html(msg);
			}
			session.subscribe('com.example.chat', chathandler);
			session.subscribe('com.example.lobby', lobbyhandler);
			
        };
         connection.open();
};
// end Web Socket Setup--------------------------------------------------------
