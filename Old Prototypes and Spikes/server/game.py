from twisted.python import log
from player import *
import time
import random
from random import shuffle
from gamemap import *
from riskyserv import *
from lobby import *

gameid = 64107	# initial game id

class game:
	'''Class for each game, multiple games can run concurrently'''
	def __init__(self,name="",privacy=False,owner=None):
		global gameid
		self.clients = []
		self.players = dict()	# sid:[name,wsclient]
		self.turnOrder = []		# [sid,sid] generated during start method by generateTurnOrder method
		self.board = gamemap()
		self.gid = str(gameid)
		gameid +=1
		self.name = name
		self.password=privacy
		self.state = 0		# waiting for players, 1 = place units mode, 2 = ingame, 3 = game completed - additional states could be used for which player input is required from etc
		self.owner = owner	# id of player that created the game
		self.lobby = None
		
	def start(self):
		'''Sets game to start mode, generating all the initial player controlled countries list and player turn order'''
		self.assignArmyUnits()
		self.initialise()			# generates game locations and assigns players initial ownership
		self.generateTurnOrder()
		self.state = 1				# proceed to place army mode

	def assignArmyUnits(self):
		'''assigns starting army units to players based on how many players there are as per risk rules'''
		global playerById
		initialPieces = {2:40,3:35,4:30,5:25,6:20}
		unitsEach = initialPieces.get(len(self.players))
		for p in self.players.keys():
			plr = playerById.get(p)
			plr.reinforcements=unitsEach


	def generateTurnOrder(self):
		'''generates player turn order, not normally called directly, but called during start method'''
		for user in self.players.keys():
			self.turnOrder.append(user)

		shuffle(self.turnOrder)

	#retrieve the turn order as is stored on the server
	def getTurnOrder(self):
		return self.turnOrder

	def initialise(self):
		'''initialses game locations, called by start method'''
		global playerById
		initGame = self
		defaultList = self.board.defaultLocations

		#makes a list of all available country indexes
		availableCountries = range(len(self.board.defaultLocations))

		#loop while there are countries available
		while len(availableCountries) >= 1:
			#loop through the list of players dividing countries equally
			for player in self.players.keys():
				#if only one remains assign and exit loop
				if len(availableCountries) == 0:
					break
				#assign country to player then remove that reference from the available list
				ran = random.randint(0, len(availableCountries)-1)
				defaultList[availableCountries[ran]][1] = player
				playerById.get(player).reinforcements -=1		# reduce available reinforcements
				del availableCountries[ran]

		#convert the output into a dictionary so that values can be obtained faster, as to avoid
		#iterating through each individual list
		storageDictionary = {}
		for items in defaultList:
			storageDictionary[items[0]] = [items[1], items[2]]

		self.board.locations = storageDictionary
		log.msg("generated initial gamestate for game: %s" % self.gid)



	def attack(self,uid,attackfrom,attackto):
		'''this statement deals with if one country is attack one another returns the result of the combat'''
		modifyState = self.board

		#get current game state values
		curState = self.board.locations

		#determine the attack and defending amounts
		attackingAmount = self.board.locations.get(attackfrom)[1]
		defendingAmount = self.board.locations.get(attackto)[1]

		#generate the combat dice lists for each side
		attackingRolls = self.rolldice(attackingAmount, "attack")
		defendingRolls = self.rolldice(defendingAmount, "defend")

		#keep track of losses on each side
		attackerLosses = 0
		defenderLosses = 0

		while len(defendingRolls) >0:
			#depending on outcome increment loss totals
			if attackingRolls[0] > defendingRolls[0]:
				defenderLosses += 1
			else:
				attackerLosses += 1

			#remove checked totals from lists and continue until battle is completed
			del attackingRolls[0]
			del defendingRolls[0]

		self.board.locations.get(attackfrom)[1] -= attackerLosses
		self.board.locations.get(attackto)[1] -= defenderLosses


		if self.board.locations.get(attackto)[1] < 1:
			amountToMove = self.board.locations.get(attackfrom)[1] - 1
			newOwner = self.board.locations.get(attackfrom)[0]

			self.board.locations[attackto] = [newOwner, amountToMove]
			self.board.locations[attackfrom][1] = 1

		return [str(attackerLosses), str(defenderLosses)]

	def placearmy(self,uid,country,amount=1):
		'''places army units on player owned countries, olny callable in place army mode (game state 1)'''
		if not self.state == 1:
			log.msg("Error: %s attempted to place army unit while not in place army mode, current game state: %d" % (uid,self.state))
			return False
		else:
			if self.board.locations.get(country)[0] == uid:		# check if country owned by player
				self.board.locations.get(country)[1] = int(self.board.locations.get(country)[1]) + int(amount)
				playerById.get(uid).reinforcements -= int(amount)	# reduce available units by amount places
				if debug>3:
					log.msg("Info: Placed %d units at %s, there are now %d units there" % (int(amount),country,self.board.locations.get(country)[1]))
					log.msg("Info: Player %s id (%s) now has %s reinforcements" % (str(playerById.get(uid).name),str(uid),str(playerById.get(uid).reinforcements)))
				return True
			else:
				log.msg("Error: %s attempted to place army unit at %s which is owned by %s" % (uid,country,self.board.locations.get(country)[0]))
				return False

	#this deals with end of turn reinforcement by a player
	#updates the game state accordingly and returns it to each user
	def reinforce(self,uid,countryfrom,countryto,amount=1):
		if not self.board.locations.get(countryfrom)[0]==uid:
			log.msg("Error: %s cannot reinforce from %s because they do not own it" % (uid,countryfrom))
			return False
		if not self.board.locations.get(countryto)[0]==uid:
			log.msg("Error: %s cannot reinforce	 %s because they do not own it" % (uid,countryto))
			return False
		if not int(self.board.locations.get(countryfrom)[1])>1:
			log.msg("Error: %s cannot reinforce from %s because there is only 1 unit there" % (uid,countryfrom))
			return False
		self.board.locations.get(countryfrom)[1] = int(self.board.locations.get(countryfrom)[1]) - 1	# take one from first country
		self.board.locations.get(countryto)[1] = int(self.board.locations.get(countryto)[1]) +1			# give it to second country
		log.msg("%s reinforced %s from %s, there are now %d units at %s" % (uid,countryto,countryfrom,int(self.board.locations.get(countryto)[1]),countryto))
		return True

	def nextPlayer(self,uid):
		'''returns the id of the next player'''
		if self.turnOrder.index(uid) == len(self.turnOrder)-1:
			return self.turnOrder[0]
		else:
			return self.turnOrder[self.turnOrder.index(uid)+1]


	def hasWon(self,uid):
		'''check to see if a player has won the game'''
		if all(x[0]==uid for x in self.board.locations.values()):		# all countries owned by player
			return True
		#if not return false, the game continues
		else:
			return False

	def rolldice(self,armieSize, type):
		rollAmount = 0
		#type represents attack or defend
		if (armieSize == 1) and (type == "attack"):
			rollAmount = 1

		elif (armieSize == 2) and (type == "attack"):
			rollAmount = 2

		elif (armieSize > 2) and (type == "attack"):
			rollAmount = 3

		elif (armieSize == 1) and (type == "defend"):
			rollAmount = 1

		elif (armieSize == 2) and (type == "defend"):
			rollAmount = 2

		elif (armieSize > 2) and (type == "defend"):
			rollAmount = 2

		diceroll = []

		#roll the random dice rolls
		for i in range(rollAmount):
			diceroll.append(random.randint(1,6))

		#sort from highest to lowest
		diceroll.sort(reverse=True)

		return diceroll
		
	def cullPlayer(self,id):
		if not id in self.players:
			return				# cant cull non-existant player
		if not len(self.players.keys())<=2:
			others = [x for x in self.players.keys() if x != id]
			owned = [x for x in self.board.locations.keys() if self.board.locations.get(x)[0] == id]
			if debug >2: log.msg("Warn: Player %s was in game %s now reassigning their owned territories to remaining players" % (id,str(self.gid)))
			shuffle(owned)
			shuffle(others)
			i=0
			while len(owned) > 0:
				c = owned.pop()
				p = others[i]
				c2 = self.board.locations.get(c)
				c2[0] = p
				c2[1] = 1
				if debug > 3: log.msg("Info: giving %s to %s" % (c,others[i]))
				i+=1
				if i == len(others):
					i=0
			del self.players[id]
			if debug >2: log.msg("Warn: Culled user id %s from game %s" % (id,str(self.gid))) 
		else:
			self.endGame()
		return True
		
	def endGame(self):
		self.lobby.gameOver(self.gid)