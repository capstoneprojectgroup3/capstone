from twisted.python import log
from game import *
from player import *
from riskyserv import *

class lobby:
	'''Main game object that holds all players and games'''
	def __init__(self):
		self.players = []
		self.games = []
		self.clients=set()
		self.chatClients=set()

	def getGameById(self,gmid):
		'''returns a game object if there is one matching the givin gameid, otherwise None'''
		for g in self.games:
			if str(g.gid) == str(gmid):
				return g
		log.msg("Error:game %s not found" % str(gmid))
		return None

	def chatAll(self,sid,msg):
		'''Sends a chat mesage to all websock clients in the chatClients set'''
		#
		# todo
		# periodicaly prune chatcCients of defunct clients(ones that have refreshed their browser or disconnected uncleanly)
		# so that chatAll has no chance of error
		#
		global clientById,nameById
		if debug: log.msg("sending chat message: %s" % msg)
		for uid in self.chatClients:
			clientById.get(uid).sendMessage("chat;%s:%s" % (nameById.get(sid),msg))

	def newPlayer(self,name,sid,ws):
		'''	Adds a new player object to the lobby that sets references to their name, id and websock client, returns sessionid
			also adds the player object to the playerById global dictionary'''
		global playerById
		p = player(name,str(sid),ws)
		self.players.append(p)
		playerById[str(sid)]=p
		return '%s' % (str(sid))

	def remPlayer(self,sid):
		for p in self.players:
			if str(p.sid) == str(sid):
				del p
				if debug: log.msg("Lobby: removed,%s" % str(sid))
				break
		return "removed,%s" % str(sid)

	def getPlayerById(self,sid):
		for p in self.players:
			if str(p.sid) == str(sid):
				return p
		log.msg("Error: Player not found")
		return None

	def getPlayers(self):
		tp=[]
		tp[:]=[]
		for p in self.players:
			tp.append(p.name)
		return(",".join(tp))

	def getsid(self,name):
		s=-1
		for p in self.players:
			if p.name == name:
				s=p.sid
				break
	# note: returns -1 if not found
		return str(s)

	def newGame(self,initiatingplayersid,gamename="",privacy=""):
		'''Creates a new game inside the lobby, returns the game id that is generated'''
		global gameid,clientById
		g = game(gamename,privacy,initiatingplayersid)
		self.games.append(g)
		g.players[initiatingplayersid]=[self.getPlayerById(initiatingplayersid).name,clientById.get(initiatingplayersid)]
		g.clients.append(clientById.get(initiatingplayersid))
		gameid += 1
		g.lobby=self
		return g.gid

	def getGameStatus(self,gid):

		return str(self.getGameById(gid).status)
		
	def joinGame(self,playerid,gameid):
		global nameById,clientById,playerById
		g = self.getGameById(gameid)
		if not g:
			if debug >0: log.msg("Fatal: Player %s tried to join non-existing game with id %s" % (nameById.get(playerid),gameid))
			return False
		if playerid in g.players.keys():
			if debug > 0: log.msg("Fatal: Player %s tried to join game %s when they are already in it" % (nameById.get(playerid),gameid))
			return False
		p = playerById.get(playerid)
		if not p:
			if debug > 0: log.msg("Fatal: Player with id %s cannot be found while trying to join game %s" % (playerid,gameid))
			return False
		p.matchmaking=True
		g.players[playerid]=[p.name,clientById.get(playerid)]
		return True
	
	def gameOver(self,gid):
		global nameById
		g = self.getGameById(gid)
		if len(g.players.keys()) != 0:
			winner=""
			x = nameById.get(g.board.locations.get("australia")[0])
			if x != None:
				winner += x
			for id in g.players.keys():
				c = clientById.get(id)
				p = playerById.get(id)
				if c != None:
					c.sendMessage("gamefinished;%s;%s;%s" % (id,str(gid),winner))
		else:
			if debug>1: log.msg("Error: game %s ended with no players in it, hence no winner" % str(gid))
		if debug>2: log.msg("Warn: Game %s is over and is being culled" % str(gid))
		del g
		
		
		