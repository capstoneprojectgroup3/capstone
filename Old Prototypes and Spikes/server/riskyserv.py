#!/usr/bin/env python
'''	group 3 capstone 2014

	riskyserv.py
	A risk-like game implemented in event driven client-server model using python and websockets for the server
	and javascript and html5 websockets on the client side.
	
	This is the main file, additional file in this project are:
		lobby.py game.py player.py gamemap.py
	
	Dependencies:
		The following libraries are required on the machine that runs this software
		python: twisted (twisted-matrix.com) which also requires zope interface(https://pypi.python.org/pypi/zope.interface#download)
				autobahn (autobahn.ws)
				
		to easily install these dependencies with pip:

			pip install zope.interface twisted autobahn
'''

import sys
import hashlib
import time

from twisted.python import log
from twisted.internet import reactor
from twisted.internet import task
from autobahn.twisted.websocket import WebSocketServerProtocol, WebSocketServerFactory
from twisted.internet.task import LoopingCall
										
clientById = dict()
nameById = dict()
playerById = dict()
	
from lobby import *
from game import *
from player import *

# debug modes: 0 off,1 fatal, 2 error, 3 warn, 4 info and RAW for debug
debug = 4 	# 4 is maximum information from logging events

class RiskyProtocol(WebSocketServerProtocol):
	'''websocket protocol handler, responds to websocket events, game messages are passed to responder method'''
	global clientById,nameById,playerById

	def onConnect(self, request):
		if debug > 2: log.msg("Warn: New connection from: {0}".format(request.peer))
		MainLobby.clients.add(self)

	def onOpen(self):
		if debug > 3 : log.msg("Info: WebSocket connection open.")

	def onMessage(self, payload, isBinary):
		if isBinary:
			if debug > 0: log.msg("Fatal: Binary message received when expecting text: {0} bytes".format(len(payload)))
		else:
			if debug>3: log.msg("Raw: %s" % payload)
			self.responder(payload)

	def onClose(self, wasClean, code, reason):
		if debug > 0: log.msg("Fatal:connection closed: {0}".format(reason))

	def responder(self,line):
		'''responds to client initiated game events
		all communucations between game client and server are handled here.
		'''
		global clientById,nameById,playerById		# global indexes for client, name and player lookups
		line=line.split(';')
		if not len(line)==4:				  # all requests are in 4 sections, see API documentation
			self.sendMessage('Error: Invalid request')
			if debug > 1: log.msg("Error: invalid request due to only %d elements (requires 4)" % len(line))
			return
			
		if line[1] in playerById:
			playerById.get(line[1]).moved()
			if debug > 3: log.msg("Info: Updated player %s lastseen" % line[1])
			
		if line[0] == 'newuser':
			hash_object = hashlib.md5(line[2].encode())		# encodes email into hash
			sessionid = str(hash_object.hexdigest())

			if not clientById.has_key(sessionid):
				sid=MainLobby.newPlayer(line[3],sessionid,self)			# new player called line[3]
				self.sendMessage("newuser;%s" % sessionid)				# return new players session id to track requests
				clientById[sessionid]=self
				nameById[sessionid]=line[3]
				for c in MainLobby.chatClients:
					clientById.get(c).sendMessage("lobbyuser;%s" % line[3])		# perhaps later client can do next line without the send .....
					clientById.get(c).sendMessage("chat;%s has joined the lobby" % line[3])	 # add a message that player has joined the lobby
				MainLobby.chatClients.add(sessionid)
				if debug>2: log.msg("Warn:new user %s with id %s joined lobby connecting from %s" % (line[3],sessionid,self.peer))
			else:
				self.sendMessage("newuser;%s" % sessionid)
				if debug > 2: log.msg("Warn: Client exists, updated connection info for id %s" % sessionid)	# note: check client side error handling for errors responded
				if clientById.get(sessionid) in MainLobby.clients:
					MainLobby.clients.remove(clientById.get(sessionid))	# remove stale client
				clientById[sessionid]=self

		if line[0] == 'newgame':
			gid = MainLobby.newGame(line[1],line[3])
			if debug > 3: log.msg("Info: New game called by %s gameid assigned: %s" % (nameById.get(line[1]),str(gid)))
			MainLobby.getGameById(str(gid)).clients.append(self)
			pl = MainLobby.getPlayerById(line[1])
			pl.matchmaking = True
			pl.initiator = True
			self.sendMessage("mygame;%s;%s" % (gid,line[3]))
			for c in MainLobby.chatClients:
				if c != line[1]:
					clientById.get(c).sendMessage("newgame;%s;%s" % (gid,line[3]))
					
		if line[0] == 'newprivgame':			# passworded game
			gameid = MainLobby.newGame(line[1],line[2],line[3])
			MainLobby.getGameById(gameid).players[line[0]]=[nameById(line[0]),self]
			pl = MainLobby.getPlayerById(line[0])
			pl.matchmaking = True
			pl.initiator = True
			self.sendMessage("ok,%d" % gameid)
			
		if line[0] == 'sendchat':		 #lobby chat
			MainLobby.chatAll(line[1],line[3])
			self.sendMessage("ok")
			if debug >3: log.msg("Info: Sending chat message to all chat clients")
			
		if line[0] == 'joingame':
			if debug > 3: log.msg("Info: joingame recieved from %s joining game %s" % (line[1],line[3]))

			joined = MainLobby.joinGame(line[1],line[3])
			if joined:
				g = MainLobby.getGameById(line[3])
				for clientid in g.players.keys():
					c = clientById.get(clientid)
					c.sendMessage("joingame;%s;%s;%s" % (line[3],line[1],nameById.get(line[1])))
					c.sendMessage("chat;%s is joining %s" % (nameById.get(line[1]),g.name))
			else:
				if debug > 0: log.msg("Fatal: %s cannot join game %s" % (line[1],line[3]))
				

		if line[0] == 'startgame':
			''' do initial game setup routine'''
			g = MainLobby.getGameById(line[2])
			p = playerById.get(line[1])
			if p.initiator==True:
				g.state=1		   
				g.start()	# initial countrys, initial player turn

				# remove users from chat channel(lobby)
				order = g.turnOrder
				out = []
				for o in order:
					out.append(o + '":"' + nameById.get(o))
				for c in g.players.keys():			# crazy quotes for json
					clientById.get(c).sendMessage('startgame;%s;{"%s"};%s' % (line[2],'","'.join(out),",".join([nameById.get(x) for x in order] )))
			else:
				self.sendMessage("Error: You are not the game owner")
				if debug > 1: log.msg("Error: %s tried to start game %s but they are not game owner" % (line[1],line[2]))

		if line[0] == 'lobbylist':
			self.sendMessage("lobbylist;%s" % MainLobby.getPlayers())
			if debug > 3: log.msg("Info: Sending lobbylist(player names) to %s" % line[1])

		if line[0] == 'initgamestate':
			'''returns: initgamestate;<country owners id>,<country owners id>;<first player id>'''
			if debug >3: log.msg('Info: Initgamestate called from %s' % line[1])

			returnList = []
			out = MainLobby.getGameById(line[2]).board.locations.keys()
			out.sort()

			for x in out:
				returnList.append(MainLobby.getGameById(line[2]).board.locations.get(x)[0])

			for client in MainLobby.getGameById(line[2]).players.keys():
				clientById.get(client).sendMessage("initgamestate;%s;%s" % (",".join(returnList),MainLobby.getGameById(line[2]).turnOrder[0]))

		if line[0] == 'startingcards':
			print 'starting cards called'
			#
			# todo
			# maybe redundant code, could be implemented under start

		if line[0] == 'attack':
			#passed information in the form
			#attack;sessionid;gameid;attackingcountry,defendingcountry
			attacker = line[1]
			details = line[3]

			detailsList = details.split(",")

			attackfrom = detailsList[0]
			attackto = detailsList[1]

			if debug > 3: log.msg("Info: Received attack by %s going from %s which has %d units to %s which has %d units" % (attacker,attackfrom,MainLobby.getGameById(line[2]).board.locations.get(attackfrom)[1],attackto,MainLobby.getGameById(line[2]).board.locations.get(attackto)[1]))

			result = MainLobby.getGameById(line[2]).attack(attacker,attackfrom,attackto)
	
			for client in MainLobby.getGameById(line[2]).players.keys():
				clientById.get(client).sendMessage("attack;%s,%s,%s,%s" % (attackfrom,attackto,result[0],result[1]))
				# results are losses from attacker and defender
			if debug >3: log.msg("Info: Units per country after attack - %s %d, %s %d" % (attackfrom,MainLobby.getGameById(line[2]).board.locations.get(attackfrom)[1],attackto,MainLobby.getGameById(line[2]).board.locations.get(attackto)[1]))
			
			
			# if a country has been conquered
			if MainLobby.getGameById(line[2]).board.locations[attackto][1] <1:
				MainLobby.getGameById(line[2]).board.locations[attackto][0] = attackfrom	# set new owner of country
				for client in MainLobby.getGameById(line[2]).players.keys():
					clientById.get(client).sendMessage("newowner;%s,%s" % (MainLobby.getGameById(line[2]).board.locations[attackfrom][0],attackto))
					
		if line[0] == 'reinforce':
			'''reinforces a countries army from an adjacent country
			reinforce;<sessionid>;<gameid>;<countryfrom>,<countryto>,<units>
			'''
			details = line[3].split(',')
			if debug >3: log.msg("Info: received reinforce by %s from %s to %s of amount %s" % (line[1],details[0],details[1],details[2]))
			reinforced = MainLobby.getGameById(line[2]).reinforce(line[1],details[0],details[1],details[2])
			if reinforced:
				for client in MainLobby.getGameById(line[2]).players.keys():
					clientById.get(client).sendMessage("reinforce;%s;%s;%s" % (details[0],details[1],details[2]))
			else:
				clientById.get(line[1]).sendMessage("Error: cannot reinforce, something went wrong on server")

		if line[0] == "placeunit":
			'''adds armies to the specified country'''
			details = line[3].split(',')
			if debug > 3: log.msg("Info: Received unit placement by %s on %s amount %s" % (line[1],details[0],details[1]))
			placed = MainLobby.getGameById(line[2]).placearmy(line[1],details[0],details[1])
			if not placed:
				self.sendMessage("Error: Invalid move")
			else:
				for p in MainLobby.getGameById(line[2]).players.keys():
					client = clientById.get(p)			# look up websock client from global dict
					client.sendMessage("placeunit;%s;%s" % (";".join(details),MainLobby.getGameById(line[2]).nextPlayer(line[1])))

		if line[0] == 'turnend':
			'''return the next user in the list'''
			for p in MainLobby.getGameById(line[2]).players.keys():
				client = clientById.get(p)			# look up websock client from global dict
				nextPlayer = str(MainLobby.getGameById(line[2]).nextPlayer(line[1]))
				client.sendMessage("turn;%s" % nextPlayer)
				if debug > 3: log.msg("Info: Turn ended by %s, next player is %s" % (line[1],nextPlayer))

		if line[0] == 'drawcard':
			'''returns a gamecard, used when a player has conquered a territory'''
			if debug >3:  log.msg("Info: Received card request from %s note - this method is not implemented on server yet" % line[1])
			#
			# todo:
			# Implement this function providing we are still providing cards in the game

		if line[0] == 'gamelist':
			out =[]
			mygame= False
			for x in MainLobby.games:
				out.append(x.gid)
				out.append(x.name)
				if x.owner==line[1]:				# check if player has created a game already
					mygame=x
			self.sendMessage("gamelist;%s" % ",".join(out))
			if mygame:								# send player their game id
				self.sendMessage("mygame;%s;%s" % (mygame.gid,mygame.name))
				if debug >2: log.msg("Warn: User %s requested gamelist so resent mygame" % line[1])
		
def resendGameState(gid):
	g = MainLobby.getGameById(gid)
	if len(g.players.keys()) ==0:
		return					# can't send gamestate to no users
	clist = g.board.locations.keys()
	clist.sort()
	out = ""
	for x in clist:
		out += x + "," + g.board.locations.get(x)[0] +"," + str(g.board.locations.get(x)[1]) + ";"
	for p in g.players.keys():
		c = clientById.get(p)
		if c != None:
			c.sendMessage("gamemap;%s" % out)
	if debug>3: log.msg("Info: sent gamemap to remaining users in game %s" % str(gid))

def cullPlayers():
		timenow = time.time()
		for p in playerById.values():
			id = p.sid
			if (timenow-p.lastseen) > 300:		# if player has been idle for more than 5 minutes 
				if debug >2: log.msg("Warn: Player %s id %s is idle past timeout and is being culled" % (p.name,id))
				for g in MainLobby.games:
					if id in g.players:
						g.cullPlayer(id)		# cull player from any game they are in
						resendGameState(g.gid)
				cli = clientById.get(id)
				MainLobby.clients.discard(cli)		# cull player from lobby
				MainLobby.chatClients.discard(id)
				del nameById[id]
				del clientById[id]
				del playerById[id]
			

if __name__ == '__main__':
	log.startLogging(sys.stdout)
	MainLobby = lobby()
	lc = task.LoopingCall(cullPlayers)
	lc.start(30.0) # call cullPlayers every 30, this is likely to be performance intensive with many games and users logged in, so may be reduced in future for scaling
	factory = WebSocketServerFactory("ws://localhost:8080", debug = False)
	factory.protocol = RiskyProtocol
	reactor.listenTCP(8080, factory)
	reactor.run()