import time
from twisted.python import log

class player:
	def __init__(self,name=None,sid=None,wsclient=None):
		self.name = name
		self.sid = sid
		self.reinforcements=0
		self.cards=[]
		self.ingame = False
		self.matchmaking = False
		self.wsclient = wsclient
		self.lastseen = time.time()
		self.initiator = False

	def addReinforcements(self,r):
		if not isinstance(r,int):
			log.msg("Error: addReinforcements cannot add a non-integer amount of reinforcements")
			return False
		else:
			self.reinforcements +=int(r)
			log.msg("added %d reinforcements to %s now there's %d" % (r,self.name,self.reinforcements))

	def addCard(self,c):
		self.cards.append(c)

	def moved(self):
		self.lastseen = time.time()		#update last seen because player has just performed an action