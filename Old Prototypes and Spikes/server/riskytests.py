import unittest
from player import *
from game import *
from lobby import *
from riskyserv import *


class TestRiskyObjects(unittest.TestCase):
	"""unit tests for game objects"""
	global playerById

	def setUp(self):
		lob = lobby()
		lob.newPlayer("p1", "5687987", "ws_client_here") # note, adjust this later when game.players.<object> becomes playerid strings in a list
		lob.newPlayer("p2", "6782678", "some_other_ws_client")
		gam = game("test_game")
		
		
		
	def test_playerClassMemberTypes(self):
		"""checks player members are the correct types"""
		p = playerById.get("5687987")

		self.assertTrue(isinstance(p, player))
		self.assertTrue(isinstance(p.sid, str))
		self.assertTrue(isinstance(p.name, str))
		self.assertTrue(isinstance(p.cards, list))
		self.assertTrue(isinstance(p.ingame, bool))
		self.assertTrue(isinstance(p.matchmaking, bool))
		self.assertTrue(isinstance(p.reinforcements, int))
		self.assertTrue(isinstance(p.wsclient, str))  # should be a ws client here but for this test is just a string
		self.assertTrue(isinstance(p.lastseen, float)) # a float representing seconds since epoch
		self.assertTrue(isinstance(p.initiator, bool))

	def test_addReinforcements(self):
		"""reinforcements increase correctly when addReinforcements called"""
		p = playerById.get("5687987")
		val = p.reinforcements
		p.addReinforcements(5)
		self.assertTrue(val + 5 == p.reinforcements)

	def test_invalid_addReinforcements(self):
		"""add_reinforcements ignores invalid argument types"""
		p = playerById.get("5687987")
		val = p.reinforcements
		p.addReinforcements('d')
		p.addReinforcements('lu')
		p.addReinforcements('1.783')
		self.assertTrue(val == p.reinforcements)
	
	def test_playerMoved(self):
		"""Moved updates time float in a reasonable timeframe"""
		p = playerById.get("5687987")
		timebefore = time.time()
		
		def waitamo():
			for x in range(10000):		# wait a little because time isn't granular enough
				x = x
		waitamo()
		p.moved()
		waitamo()		# wait a little because time isn't granular enough
		timeafter = time.time()
		self.assertTrue(timebefore < p.lastseen and	 p.lastseen < timeafter)
		
	def test_lobbyNewGame(self):
		""" -- """
		

if __name__ == '__main__':
	unittest.main()
