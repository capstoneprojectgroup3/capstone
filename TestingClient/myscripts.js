// My information
var me = {myId:0 ,myGameName:'', myColor:''};

// CARDS
var myCards = {1:"", 2:"", 3:"", 4:"", 5:"", 6:""}; // contains cards collected by player
var freeMyCardsKeys = [1,2,3,4,5,6];
var temp_freeMyCardsKeys = [];
var cardsChosen = []; // ARRAY - add 3 selected cards to this
var cards = [['wild', 3], ['alaska', 0], ['northernterritory', 1], ['ontario', 2], ['easternrussia', 0], ['quebec', 1], ['easternunitedstates', 2], ['centralamerica', 0], ['venezuela', 1], ['peru', 2], ['brazil', 0], ['argentina', 1], ['greenland', 2], ['iceland', 0], ['greatbritain', 1], ['westerneurope', 2], ['northerneurope', 0], ['southerneurope', 1], ['scandinavia', 2], ['kamchatka', 0], ['japan', 1], ['newguinea', 2], ['indonesia', 0], ['australia', 1], ['newzealand', 2], ['madagascar', 0], ['southafrica', 1], ['northafrica', 2], ['egypt', 0], ['eastafrica', 1], ['congo', 2], ['russia', 0], ['middleeast', 1], ['afghanistan', 2], ['ural', 0], ['yakutsk', 1], ['mongolia', 2], ['siberia', 0], ['india', 1], ['china', 2], ['thailand', 0], ['westernunitedstates', 1], ['alberta', 2]];

var images = ["img/solider.png","img/tank.png","img/jet.png","img/wild.png"];
// END CARDS


// Global variables
var gameStarted = 0; // flag to indicate if users can use map
var gamePhase = 1; // flag to indicate whether game in 1: place army 2: attack mode 3: game over 4: in-game place-army
						

var gameId = 0; 		// INT - the id of this specific game
var gameName = ''; 		// STRING - the name of this specific game
var playersDict = {}; 	// OBJECT - {id:name}
var listOfUsers = []; 	// ARRAY - list of the users in the game - stored as sessionId's
var session; 			// A variable for connection to server
var itsMyGame = 0; 		// Flag to indicate whether I made the game

var myTurn = 0; 		// Flag to indicate if it is my turn
var firstTurn = 1; 		// Flag to indicate whether this is my first country choice in this turn
var firstCountry; 		// The caller object of the first country I chose in this turn
var choosingCards = 0; 	// Flag to indicate if players can choose

var colors = ["#3498db","#e74c3c","#2ecc71","#e67e22","#9b59b6","#f1c40f"];
var unitCount = 0;

var canInviteUsers = 0;             //used for once a private game has been made, allows users to invite other users to there game
var canInviteUsersGameName = '';    //just a simple variable that will hold some info to make the inviting process slightly easier

//END Global variables

// Card Functions	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
function chooseCards(){ // remember to make the button hide once any other thing is done
	if(myTurn){
		gameStarted = 0;
		choosingCards = 1;
		temp_freeMyCardsKeys = freeMyCardsKeys.slice(0); // make a copy incase player changes mind or gives incorrect cards.
	}
	
};
function stopChooseCards(){
	gameStarted = 1;
	choosingCards = 0;		
	freeMyCardsKeys = temp_freeMyCardsKeys.slice(0);
	deselectCards();
}
function deselectCards(){
	cardsChosen = [];
	for (var i = 1; i < 6; i++){
		d3.select("#card"+i.toString()).style("background-color","#E6E6E6");
        d3.select("#card"+i.toString()).style("opacity","1");
	}	
}
function cardChosen(cardVal){ // cardVal is INT value of submitting div
	if (myCards[cardVal] != '' && (cardsChosen.indexOf(cardVal) < 0)) {
		if(cardsChosen.length == 2){
			cardsChosen.push(cardVal)
			freeMyCardsKeys.push(cardVal);
			checkCards(cardsChosen[0],cardsChosen[1],cardsChosen[2]);
		} else {
			cardsChosen.push(cardVal)
			freeMyCardsKeys.push(cardVal);
			d3.select("#card"+cardVal.toString()).style("opacity","0.5");
		}
	}
};

function checkCards(first,second,third){
	var valid = 0;
	// a - b - c are the card class(soldier, tank or jet).
	a = cards[myCards[first]][1];
	b = cards[myCards[second]][1];
	c = cards[myCards[third]][1];
	if(a==3||b==3||c==3){
		valid = 1;
	} else {
		// check whether unique or same triples
		if(a==b&&a==c){// check same
			valid = 1;
		} else if(b==a&&b==c){
			valid = 1;
		} else if(c==a&&c==b){
			valid = 1;
		} else if(a!=b&&a!=c&&b!=c){//check unique
			valid = 1;
		} else {
			valid = 0;
		}
	}
	if(valid){
		var cardValList = [first,second,third];
		var i = 0;
		for(i=0;i<3;i++){
			document.getElementById("pic"+cardValList[i].toString()).src = '';
			document.getElementById("country"+cardValList[i].toString()).innerHTML = '';
			d3.select("#card"+cardValList[i].toString()).style("background-color","#E6E6E6");
			d3.select("#card"+cardValList[i].toString()).style("display","none");
			myCards[cardValList[i]]='';
		}
		stopChooseCards();
		tradeCards(a,b,c);
	} else {
		cardsChosen = [];
		freeMyCardsKeys = temp_freeMyCardsKeys.slice(0);
		//alert("Incorrect Card Combination");
        flashIncorrectCardCombo();
	}
};

function updateCardUI(card,i){
	console.log("updating card graphics");
	cardInfo = cards[card];
	document.getElementById("pic"+i.toString()).src = images[cardInfo[1]];
	document.getElementById("country"+i.toString()).innerHTML = window[cardInfo[0]].displayName;
	//d3.select("#card"+i.toString()).style("background-color","#ffcc00");
	d3.select("#card"+i.toString()).style("border-radius","15px");
    d3.select("#card"+i.toString()).style("display","inherit");
};
// END Card Functions	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	

// Functions activated by selecting map	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	
function myFunc(caller){ // when you click on the map this function is called
	if(gameStarted){
		if(unitCount > 0){
			if(myTurn&&window[caller.id].owner==me.myId){
				unitCount -=1;
				if(unitCount==0){
					flashAttackStage();
					gamePhase = 2;
					document.getElementById("gameStage").value = "ATTACK";
					document.getElementById("gameStage").style.color = "#F30021";
				}
				document.getElementById("deployMeter").value = unitCount;
				//setMyTurn(0);
				placeUnit(caller.id);
			}
		} else if(myTurn){ // its my turn and I can attack or reinforce
			if(firstTurn){
				firstChoice(caller);
			} else {
				console.log(window[firstCountry.id].neighbours.indexOf(caller.id),"index");
				console.log(window[firstCountry.id].neighbours,"neighbours");
				if(caller.id==firstCountry.id){
					deselectCountry(firstCountry);
					firstTurn = 1;
				} else if(window[firstCountry.id].neighbours.indexOf(caller.id)>=0){
					secondChoice(caller);
				}
			}
		}
	}
	return;
};

function firstChoice(caller){
	if(window[caller.id].owner==me.myId){ 
		if(window[caller.id].unitCount>1){
			selectCountry(caller);
			firstTurn = 0; // its not my first turn anymore
			firstCountry = caller; // remember my first cell choice 
		}            
	} 
	return;
};

function secondChoice(caller){
	// check to make sure I havn't already selected this country
	if(window[caller.id].owner!=me.myId){ // if true then I must be attacking
		console.log("im trying to attack");
		if(window[firstCountry.id].neighbours.indexOf(caller.id)>=0){// check whether second selected country is neighbour
			console.log("Second country is a neighbour");
			if(window[firstCountry.id].unitCount >=2){//but to attack, I need at least two armies
				deselectCountry(firstCountry); // change the first selected country back to my color	
				firstTurn = 1;					
				attack(firstCountry,caller);
			}
		}
	} else { // I must be reinforcing
		if(window[firstCountry.id].neighbours.indexOf(caller.id)>=0){// check whether second selected country is neighbour
			console.log("The country I want to reinforce is a neighbour");
			selectCountry(caller);
			var max = window[firstCountry.id].unitCount - 1;
			var n = -1;
			while(n<0){
				n = window.prompt("How many armies would you like to move? (Max: " + max,"");
				if(n>max){
					n = -1;
				}
			}
			if(n == 0 || n == null){
				deselectCountry(caller);
				deselectCountry(firstCountry);
				firstTurn = 1;
			} else {
				deselectCountry(caller);
				deselectCountry(firstCountry);
				console.log("calling reinforce");
				reinforce(firstCountry,caller,n);
				endTurn();
				firstTurn = 1;
			}	
		}
	}
	return;
};
function selectCountry(caller)
{
	caller.setAttribute("fill", "grey");
	return;
};
function deselectCountry(caller)
{
	caller.setAttribute("fill", me.myColor);
	return;
};
function setMyTurn(t) {
	if (t) {
		$("endTurnButton").show();
		myTurn = 1;
	}
	else {
		$("endTurnButton").hide();
		myTurn = 0;
	}
};

// END Functions activated by selecting map	-	-	-	-	-	-	-	-	-	-	-	-	-

// Receive from server	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-			
function gamehandler(response){ // function to receive data from server (response is an array)
	console.log("Recieved info from server");
	console.log(response);
    if(response[0][0]=="startgame"){
        try{
            closeLobby();
            showGame();
            displayUnitCounts();
        }
        catch(err){
            console.log("Function does not exist, probably testing version of client");
        }
		var i = 0;
		for(i=0;i<response[0][1].length;i++){
			playersDict[response[0][1][i][0]]= response[0][1][i][1];
			listOfUsers.push(response[0][1][i][1]);
		}
        var cIndex = listOfUsers.indexOf(me.myGameName);
        me.myColor = colors[cIndex];
        document.getElementById("displayMyColour").value = me.myColor.toUpperCase();
        document.getElementById("displayMyColour").style.color = me.myColor;
        document.getElementById("displayMyName").style.color = me.myColor;
       
        try{
	document.getElementById("startGameButton").style.visibility="hidden";
        }
        catch(err){
            console.log("Unable to hide button most likely due to testing version of client");
        }
        
		if(itsMyGame){
			getGameState(); // calls initgamestate
		}
    } else if(response[0]==="initgamestate"){ 
		//response = ['initgamestate';[<country owners id>,<country owners id>];<first player id>;numberOfUnits]
        setUpMap(response[1]); // line 298
		//response3 = [[id1,id2...],[n1,n2...]] (list of reinforcements for each player
		unitCount = parseInt(response[3][1][response[3][0].indexOf(me.myId)])
		
        if(response[2]==me.myId){
            setMyTurn(1);
			console.log("MY TURN");
			try{
				flashItsMyTurn();
			}
			catch(err){
				console.log("Requires prototype lobby");
			}
        }
        gameStarted = 1;
        currentPlayerName = playersDict[response[2]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[2]])];
        document.getElementById("gameStage").value = "Place Armies";
        document.getElementById("gameStage").style.color = "#FF7100";
        document.getElementById("setInGameColour").style.backgroundColor = me.myColor;
		document.getElementById("deployMeter").value = unitCount;
    } else if(response[0]==="placeunit"){
        var country = response[1];
		window[country].unitCount += 1;
        var num = window[country].unitCount;
        if(response[2]==me.myId){
            if (myTurn == 0){
                try{
				flashItsMyTurn();
			}
			catch(err){
				console.log("Requires prototype lobby");
			}
                
                
            }
            setMyTurn(1);
			
        } else {
            setMyTurn(0);
        }
        document.getElementById("displayCurrentPlayer").value = playersDict[response[2]].toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[2]])];
        updateUnitCount(country,num);
		document.getElementById("deployMeter").value = unitCount;
    } else if(response[0]==="turn"){
		//['turn',nextPlayer,state,newArmies]
		gamePhase = response[2];
		
        if(response[1]==me.myId){
            setMyTurn(1);
			unitCount += parseInt(response[3]);
			try{
				flashItsMyTurn();
			}
			catch(err){
				console.log("Requires prototype lobby");
			}
        } else {
            setMyTurn(0);
        }
        currentPlayerName = playersDict[response[1]];
        document.getElementById("displayCurrentPlayer").value = currentPlayerName.toUpperCase();
        document.getElementById("displayCurrentPlayer").style.color = colors[listOfUsers.indexOf(playersDict[response[1]])];
		document.getElementById("deployMeter").value = unitCount;
    } else if(response[0]==="reinforce"){
		console.log(response);
		console.log("The above is received from server on reinforce");
        var countryFrom = response[1];
        var countryTo = response[2];
        var unitQuantity = parseInt(response[3]);
        window[countryFrom].unitCount -= unitQuantity;
		console.log(countryFrom);
		console.log(countryTo);
        window[countryTo].unitCount += unitQuantity;
        numA = window[countryTo].unitCount;
        updateUnitCount(countryTo,numA);
        numB = window[countryFrom].unitCount;
        updateUnitCount(countryFrom,numB);
    } else if(response[0]==="attack"){
        var attackingCountry = response[1];
        var defendingCountry = response[2];
		var attackerLoss = parseInt(response[3]);
		var defenderLoss = parseInt(response[4]);

        if (window[defendingCountry].unitCount-defenderLoss <= 0) {//If attacker won, the defender will have 0 or -1 armies left!
            window[defendingCountry].owner = window[attackingCountry].owner;// defenders country now owned by attacker
            window[defendingCountry].unitCount = window[attackingCountry].unitCount-1+attackerLoss; // units moved to new country
            window[attackingCountry].unitCount = 1; // attacking country now has 1 unit
            document.getElementById(defendingCountry).setAttribute("fill",colors[listOfUsers.indexOf(playersDict[window[attackingCountry].owner])]);//and set the colours
            firstTurn = 1; //you can continue to attack now
            numA = window[defendingCountry].unitCount;
            updateUnitCount(defendingCountry,numA);
            numB = window[attackingCountry].unitCount;
            updateUnitCount(attackingCountry,numB);
            createGameChatObject("Combat", window[attackingCountry].displayName + " Has taken over " + window[defendingCountry].displayName)
        } else {
            window[defendingCountry].unitCount -= defenderLoss;
			window[attackingCountry].unitCount -= attackerLoss;
            numA = window[defendingCountry].unitCount
            numB = window[attackingCountry].unitCount
            updateUnitCount(defendingCountry,numA);
            updateUnitCount(attackingCountry,numB);
        }
    } else if(response[0]==="gamefinished"){
        if(response[2]==gameId){
            gameStatus = 0;
            if(response[3]==me.myId){
                showEndGameWin();
                //alert("YOU are the victor !");
            } else {
                showEndGameLoss(response[3]);
                //alert("Player "+response[3]+" has won the game :-(");
            }
		}
    } else if (response[0] ==="gamechat"){
        try{
            createGameChatObject(response[1],response[2])
        }catch(err){
            console.log("Test client doesnt support this function");
        }
    } else if (response[0] ==="recieveCard"){
		if(response[1]==me.myId){
			var cardIndex = freeMyCardsKeys[0];
			myCards[cardIndex] = response[2];
			freeMyCardsKeys.shift();
			// send int to helper function to update card pics
			updateCardUI(response[2],cardIndex);
		}
		
	} else if (response[0] ==="recieveReinforcements"){
		if(response[1]==me.myId){
			unitCount = unitCount + response[2];
		}
		document.getElementById("deployMeter").value = unitCount;
	} else { //if we haven't interpreted the message at all, tell us about it
            console.log("Client needs to handle this:" +response[0]);
	}
	return;
};
// END Receive from server	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-			


// Send to server functions	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-
function getLobbyList(){
    session.call('com.example.lobbylist', [me.myId]).then(
                           function(res){
                               var amountOfUsers = res[0].length;
                               for (var i=0; i<amountOfUsers; i++){
                                   try{
								   createPlayerObject(res[0][i]);
                                   //document.getElementById("playerbox").innerHTML = document.getElementById("playerbox").innerHTML + "\n" + res[0][i];
                                    }
                                    catch(err){
                                        console.log("Unable to change HTML element, due to usage of test client")
                                    }
                                   
                        }
         
                    }                           
                );
}

function newUser(name,emailAddy){
    document.getElementById("displayMyName").value = name.toUpperCase();
    me.myGameName = name;
	session.call('com.example.newuser', [name,emailAddy]).then( // Send info to the server and wait for the response
	function (res) {
		me.myId = res[0];
		console.log("newUser response:", res);
                try{
					createPlayerObject(res[1]);
					document.getElementById("newUserButton").style.visibility="hidden";
                }
                catch(err){
                    console.log("Unable to hide button most likely due to testing version of client");
                }
                
                getLobbyList();
	})
	return;
};

function newGame(gName){
	gameName = gName;
	session.call('com.example.newgame', [me.myId,gameName]).then( // Send info to the server and wait for the response
	function (res) {
		gameId = res[0];
		console.log("newGame response:", res);
		mount='com.example.'+gameId;
		session.subscribe(mount,gamehandler); // Make it so I receive all server msgs that have my game id
		console.log("subscribed to game channel: " + mount);
		document.getElementById("newGameButton").style.visibility="hidden";	
		document.getElementById("joinGameButton").style.visibility="hidden";	
	})
	return;
};

function newPrivateGame(gName){
    gameName = gName
    session.call('com.example.newprivategame', [me.myId,gameName]).then( //info of private game to server and wait for response
    function (res) { 
        gameId = res[0];
        console.log("newPrivateGame response: ", res);
        mount='com.example.'+gameId;
        session.subscribe(mount,gamehandler);//subscribe to all future messages on the game channel
        console.log("subscribed to private game channel: " + mount);
        canInviteUsers = 1
        canInviteUsersGameName = gName
    })
    return;
};

function joinGame(gameToJoin){
    if(gameStarted==0){
		session.call('com.example.joingame', [me.myId,gameToJoin]).then(
		function (res) {
			gameId = res[0];
			gameName = res[1];
			console.log("joinGame response:", res);
			mount='com.example.'+gameId;
			session.subscribe(mount,gamehandler) // Make it so I receive all server msgs that have my game id
			console.log("subscribed to game channel: " + mount)	
			try {
				document.getElementById("newGameButton").style.visibility="hidden";	
				document.getElementById("joinGameButton").style.visibility="hidden";
				document.getElementById("startGameButton").style.visibility="hidden";								
				}
			catch (err) {
			}
		})
	};
	return;
};

function startGame(){
	itsMyGame = 1;
	session.call('com.example.startgame', [me.myId,gameId]);
	return;
};

function getGameState(){ //initgamestate
	console.log("calling init");
	session.call('com.example.initgamestate', [me.myId,gameId]);
	return;
};

function placeUnit(country){ //Fortify
	console.log("Placing a unit");
	session.call('com.example.placeunit', [me.myId,gameId,country]);
	return;
};

function attack(countryA,countryB){
	console.log("Attack sent to server");
	console.log(me.myId,gameId,countryA.id,countryB.id);
	session.call('com.example.attack', [me.myId,gameId,countryA.id,countryB.id]);
	return;
};

function reinforce(originCountry,countryToReinforce,unitQuantity){
	session.call('com.example.reinforce', [me.myId,gameId,originCountry.id,countryToReinforce.id,unitQuantity]);
	return;
};

function endTurn(){
	if(myTurn) {
		if(unitCount > 0){
			alert("Place your units");
			return;
		} else {
			setMyTurn(0);
			console.log("Ending turn");
			session.call('com.example.turnend', [me.myId,gameId]);
		}
	}
	return;
};

function tradeCards(cardA,cardB,cardC){
	console.log("Trading cards");
	session.call('com.example.tradecards', [me.myId,gameId,[cardA,cardB,cardC]]);
};

// END Send to server functions	-	-	-	-	-	-	-	-	-	-	-	-	-	-	-

// Connection Setup----------------------------------------------------------------------
var session;
window.onload = function() {
	// Make a new connection to the server
	var connection = new autobahn.Connection({
	url: "ws://127.0.0.1:8080",
	realm: 'realm1'
	});
	
	connection.onopen = function (ses) {
	session = ses; // When the connection is established remember the session I have with the server
	console.log("connected");
	};
	
	connection.open(); // Keep the connection open
};
// END Connection Setup--------------------------------------------------------

function setUpMap(state){ // receive players map distribution from server and update country info
    var i = 0;
    for(i=0;i<42;i++){   
        window[countryList[i]].owner = state[i];
        var newColor = colors[listOfUsers.indexOf(playersDict[state[i]])];
        document.getElementById(countryList[i]).setAttribute("fill",newColor);
    } 
	return;    
};

// Country information		-		-		-		-		-		-		-		-		-		-		-		-		-		-		-		
countryList = ['afghanistan', 'alaska', 'alberta', 'argentina', 'australia', 'brazil', 'centralamerica', 'china', 'congo', 'eastafrica', 'easternrussia', 'easternunitedstates', 'egypt', 'greatbritain', 'greenland', 'iceland', 'india', 'indonesia', 'japan', 'kamchatka', 'madagascar', 'middleeast', 'mongolia', 'newguinea', 'newzealand', 'northafrica', 'northerneurope', 'northernterritory', 'ontario', 'peru', 'quebec', 'russia', 'scandinavia', 'siberia', 'southafrica', 'southerneurope', 'thailand', 'ural', 'venezuela', 'westerneurope', 'westernunitedstates', 'yakutsk'];
var afghanistan = {displayName:"Afghanistan", owner:'', unitCount:1, neighbours: ['middleeast', 'china', 'ural', 'russia', 'india'] };
var alaska = {displayName:'Alaska', owner:'', unitCount:1, neighbours: ['northernterritory', 'alberta','kamchatka'] };
var alberta = {displayName:'Alberta', owner:'', unitCount:1, neighbours: ['alaska', 'northernterritory', 'ontario', 'westernunitedstates'] };
var argentina = {displayName:'Argentina', owner:'', unitCount:1, neighbours: ['peru', 'brazil'] };
var australia = {displayName:'Australia', owner:'', unitCount:1, neighbours: ['newguinea', 'indonesia', 'newzealand'] };
var brazil = {displayName:'Brazil', owner:'', unitCount:1, neighbours: ['venezuela', 'peru', 'argentina','northafrica'] };
var centralamerica = {displayName:'Central America', owner:'', unitCount:1, neighbours: ['venezuela', 'westernunitedstates', 'easternunitedstates'] };
var china = {displayName:'China', owner:'', unitCount:1, neighbours: ['thailand', 'india', 'afghanistan', 'ural', 'siberia', 'mongolia'] };
var congo = {displayName:'Congo', owner:'', unitCount:1, neighbours: ['northafrica', 'eastafrica', 'southafrica'] };
var eastafrica = {displayName:'East Africa', owner:'', unitCount:1, neighbours: ['egypt', 'northafrica', 'southafrica', 'congo','madagascar'] };
var easternrussia = {displayName:'Eastern Russia', owner:'', unitCount:1, neighbours: ['mongolia', 'yakutsk', 'kamchatka', 'siberia'] };
var easternunitedstates = {displayName:'Eastern United States', owner:'', unitCount:1, neighbours: ['centralamerica', 'westernunitedstates', 'ontario', 'quebec'] };
var egypt = {displayName:'Egypt', owner:'', unitCount:1, neighbours: ['middleeast', 'norhtafrica', 'eastafrica'] };
var greatbritain = {displayName:'Great Britain', owner:'', unitCount:1, neighbours: ['iceland', 'northerneurope', 'westerneurope','scandinavia'] };
var greenland = {displayName:'Greenland', owner:'', unitCount:1, neighbours: ['quebec', 'iceland','northernterritory'] };
var iceland = {displayName:'Iceland', owner:'', unitCount:1, neighbours: ['greenland', 'greatbritain', 'scandinavia'] };
var india = {displayName:'India', owner:'', unitCount:1, neighbours: ['thailand', 'china', 'afghanistan', 'middleeast'] };
var indonesia = {displayName:'Indonesia', owner:'', unitCount:1, neighbours: ['australia', 'newguinea', 'thailand'] };
var japan = {displayName:'Japan', owner:'', unitCount:1, neighbours: ['mongolia', 'kamchatka'] };
var kamchatka = {displayName:'Kamchatka', owner:'', unitCount:1, neighbours: ['alaska','yakutsk', 'easternrussia', 'mongolia', 'japan'] };
var madagascar = {displayName:'Madagascar', owner:'', unitCount:1, neighbours: ['southafrica','eastafrica'] };
var middleeast = {displayName:'Middle East', owner:'', unitCount:1, neighbours: ['egypt', 'russia', 'afghanistan', 'india', 'southerneurope'] };
var mongolia = {displayName:'Mongolia', owner:'', unitCount:1, neighbours: ['china', 'siberia', 'easternrussia', 'kamchatka', 'japan'] };
var newguinea = {displayName:'New Guinea', owner:'', unitCount:1, neighbours: ['indonesia', 'australia', 'thailand'] };
var newzealand = {displayName:'New Zealand', owner:'', unitCount:1, neighbours: ['australia'] };
var northafrica = {displayName:'North Africa', owner:'', unitCount:1, neighbours: ['egypt', 'eastafrica', 'congo', 'westerneurope','brazil'] };
var northerneurope = {displayName:'Northern Europe', owner:'', unitCount:1, neighbours: ['westerneurope', 'southerneurope', 'russia', 'scandinavia','greatbritain'] };
var northernterritory = {displayName:'Northern Territory', owner:'', unitCount:1, neighbours: ['alberta', 'alaska', 'ontario','greenland'] };
var ontario = {displayName:'Ontario', owner:'', unitCount:1, neighbours: ['northernterritory', 'alberta', 'westernunitedstates', 'easternunitedstates', 'quebec'] };
var peru = {displayName:'Peru', owner:'', unitCount:1, neighbours: ['venezuela', 'brazil', 'argentina'] };
var quebec = {displayName:'Quebec', owner:'', unitCount:1, neighbours: ['easternunitedstates', 'ontario', 'greenland'] };
var russia = {displayName:'Russia', owner:'', unitCount:1, neighbours: ['middleeast', 'southerneurope', 'northerneurope', 'scandinavia', 'afghanistan', 'ural'] };
var scandinavia = {displayName:'Scandinavia', owner:'', unitCount:1, neighbours: ['iceland', 'northerneurope', 'russia','greatbritain'] };
var thailand = {displayName:'Thailand', owner:'', unitCount:1, neighbours: ['china', 'india','indonesia','newguinea'] };
var siberia = {displayName:'Siberia', owner:'', unitCount:1, neighbours: ['yakutsk', 'ural', 'china', 'mongolia', 'easternrussia'] };
var southafrica = {displayName:'South Africa', owner:'', unitCount:1, neighbours: ['congo', 'eastafrica', 'madagascar'] };
var southerneurope = {displayName:'Southern Europe', owner:'', unitCount:1, neighbours: ['westerneurope', 'northerneurope', 'middleeast', 'russia'] };
var ural = {displayName:'Ural', owner:'', unitCount:1, neighbours: ['russia', 'afghanistan', 'china', 'siberia'] };
var venezuela = {displayName:'Venezuela', owner:'', unitCount:1, neighbours: ['centralamerica', 'brazil', 'peru'] };
var westerneurope = {displayName:'Western Europe', owner:'', unitCount:1, neighbours: ['greatbritain', 'northerneurope', 'southerneurope', 'northafrica'] };
var westernunitedstates = {displayName:'Western United States', owner:'', unitCount:1, neighbours: ['alberta', 'ontario', 'easternunitedstates', 'centralamerica'] };
var yakutsk = {displayName:'Yakutsk', owner:'', unitCount:1, neighbours: ['siberia', 'easternrussia', 'kamchatka'] };
// END Country information		-		-		-		-		-		-		-		-		-		-		-		-		-		-		-