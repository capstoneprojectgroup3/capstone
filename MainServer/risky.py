#!/usr/bin/env python
'''	group 3 capstone 2014

    riskyserv.py
    A risk-like game implemented in event driven client-server model using python and websockets for the server
    and javascript and html5 websockets on the client side.

    This is the main file, additional file in this project are:
        lobby.py game.py player.py gamemap.py

    Dependencies:
        The following libraries are required on the machine that runs this software
        python: twisted (twisted-matrix.com) which also requires zope interface(https://pypi.python.org/pypi/zope.interface#download)
                autobahn (autobahn.ws)

        to easily install these dependencies with pip:

            pip install zope.interface twisted autobahn

        another required dependancy is pywin32:
        http://sourceforge.net/projects/pywin32/

'''	

import sys
import hashlib
import time
import traceback

pById=dict()
gById=dict()

connectedPlayers = 0

from twisted.internet.defer import returnValue
from autobahn.twisted.wamp import Application

from game import *
from player import *
from twisted.python import log

app = Application()


# debug modes: 0 off,1 fatal, 2 error, 3 warn, 4 info
debug = 4 	# 4 is maximum information from logging events

def onEvent(msg):
    print("got chat: {}".format(msg))

# helper fuctions
def playerExists(uid):
    return uid in pById

def gameExists(gid):
    return gid in gById

def mount(gid):
    return 'com.example.%s' % gid

def hasGame(uid):
    has = False
    for g in gById.values():
        if g.owner == uid:
            has = True
    return has

@app.register('com.example.triggersubscribe')
def triggerSubscribe():
    print("triggersubscribe() called")
    yield app.session.subscribe(onEvent, 'com.example.chat')

@app.register('com.example.chat')
def sc(name,message):
            '''the server receives a chat message
                which is returned to all clients
                [<name>,<message>]
            '''
            app.session.publish('com.example.chat',name,message)

@app.register('com.example.newuser')
def nu(name,email):
            ''' new user request
                returns directly to client
                [ <player id>, <player name> ]
            '''
            global pById
            global connectedPlayers
            h = hashlib.md5(email.encode())		# encodes email into hash
            uid = str(h.hexdigest())
            if uid in pById:
                p=pById[uid]
                return [p.sid,p.name]
            p=player(name,uid)
            pById[uid]=p
            connectedPlayers += 1
            app.session.publish('com.example.chat','%s has entered the lobby' % name )
            app.session.publish('com.example.lobby','usersonline',connectedPlayers)
            app.session.publish('com.example.lobby', 'user',p.name,p.sid)
            if debug > 3: log.msg("Info: New user %s" % p.name)
            return [p.sid,p.name]

@app.register('com.example.newgame')
def ng(uid,gname):
            '''User creates a game
            publishes event to the com.example.lobby channel:
            ['game',<game name>, <game id>, <game owner name>]
            '''
            if debug > 3: log.msg("Info: New game called")
            global gById,pById
            if not playerExists(uid):
                return "Error: You are not a registered user"
            p = pById[uid]
            if p.matchmaking:
                return "Error: You are already joining or have started a game"
            g = game(gname,False,uid)
            g.players.append(uid)
            gById[g.gid]=g
            p.matchmaking = True
            app.session.publish('com.example.lobby', 'game',g.name,g.gid,p.name)
            if debug > 3: log.msg("Info: New game called by %s gameid assigned %s" % (p.name,g.gid))
            return [g.gid,g.name]


@app.register('com.example.newprivategame')
def pg(uid,gname):
            '''Users creates a private game
            this is then published onto the com.example.lobby channel:
            ['privategame',<game name>,<game id>,<game owner name>]
            '''
            global gById,pById
            if not playerExists(uid):
                return "Error: You are not a registered user"
            p = pById[uid]
            if p.matchmaking:
                return "Error: You are already joining or have started a game"
            g = game(gname,False,uid)
            g.players.append(uid)
            gById[g.gid]=g
            p.matchmaking = True
            app.session.publish('com.example.lobby', 'privategame',g.name,g.gid,p.name)
            if debug > 3: log.msg("Info: New private game called by %s gameid assigned %s" % (p.name,g.gid))
            return [g.gid,g.name]

@app.register('com.example.inviteplayer')
def ip(uid,gname,invitee,gameID):
            app.session.publish('com.example.lobby','playerinvite',uid,gname,invitee,gameID)
            if debug > 3: log.msg("Info: player %s invited to game %s by %s" %(uid,gname,invitee))


@app.register('com.example.joingame')
def jg(uid,gname):
            ''' player joins a game
            publishes to both chat and game channels and also returns directly to the client
            chat: join message is sent
            game: ['join', <player name>, <player id>]
            direct: [<game id>,<game name>]
            '''
            global pById,gById
            if debug > 3: log.msg("Info: joingame recieved from %s joining game %s" % (uid,gname))
            if not playerExists(uid):
                return "Error: You are not a registered user"
            g = None
            for x in gById.values():
                if x.name == gname:
                    g = x
                    break
            p=pById[uid]
            if g==None:
                log.msg("Error: no game %s" % gname)
                return
            g.players.append(str(uid))
            log.msg("%s is joining game %s" % (p.name,g.name))
            p.matchmaking=True
            g.playersInGame += 1
            app.session.publish('com.example.lobby','lobbyplayercount',g.gid,g.playersInGame)
            app.session.publish('com.example.chat','%s is joining game %s' % (p.name,g.name))
            app.session.publish(mount(g.gid), ['join',p.name,p.sid])
            return [g.gid,g.name]

@app.register('com.example.startgame')
def sg(uid,gid):
            ''' Game owner starts gamestart
                publishes to com.example.<game id>
                [ 'startgame', [ <user id 1>, <user name 1>] , ..., ..., [<user id n>, <user name n>] ]
            '''
            global gById, pById
            if not playerExists(uid):
                return "Error: You are not a registered user"
            if not gameExists(gid):
                return "Error: Game does not exist"
            g=gById[gid]
            p=pById[uid]
            try:
                g.start()
            except:
                print "Unexpected error:", sys.exc_info()[0]
                traceback.print_exc()
                raise

            order = g.turnOrder
            out = []
            for o in order:
                out.append([o, pById[o].name])

            msg = ['startgame']
            msg.append(out)
            gs(gid)
            app.session.publish(mount(gid),msg)
            if debug > 3: log.msg('Info: startgame called by %s' % p.name)

def gs(gid):
    '''
     handling for when a game is started, this will be sent to all the clients so they can cull that game
     from their list of games
    '''
    app.session.publish('com.example.lobby', 'gamehasstarted', gid)




@app.register('com.example.lobbylist')
def ll(uid):
            global pById
            ''' user request the list of chat users
                returns directly to client
                [ <player name 1, ... , <player name n> ]
            '''
            if not playerExists(uid):
                return "Error: You are not a registered user"
            log.msg('userid:\n')
            log.msg(uid)
            plyrs = [x.name for x in pById.values() if x.name != pById[uid].name]
            log.msg(plyrs)
            if debug > 3: log.msg("Info: Sending lobbylist(player names) to %s" % pById[uid].name)
            return [plyrs]

@app.register('com.example.initgamestate')
def ig(uid,gid):
            '''returns: initgamestate;<country owners id>,<country owners id>;<first player id>;numberOfUnits'''
            if not playerExists(uid):
                return "Error: You are not a registered user"

            if not gameExists(gid):
                return "Error: Game does not exist"

            if debug >3: log.msg('Info: Initgamestate called by %s' % pById[uid].name)
            g = gById[gid]
            returnList = []
            out = gById[gid].locations.keys()
            out.sort()

            for x in out:
                returnList.append(g.locations.get(x)[0])

            #need to make a list of starting units for each player
            suList = [g.players]
            suList.append(g.playerReinforcements())
            if debug > 3:
                log.msg('Sending inital game state')
                log.msg('Starting units each: ')
                log.msg(suList)
            app.session.publish(mount(gid), 'initgamestate', returnList, g.turnOrder[0], suList)


@app.register('com.example.attack')
def atk(uid,gid,attackfrom,attackto):
            '''attcks from one country to another'''
            if not playerExists(uid):
                return "Error: You are not a registered user"
            if not gameExists(gid):
                return "Error: Game does not exist"
            g = gById[gid]
            attacker = uid

            if debug > 3: log.msg("Info: Received attack by %s going from %s which has %d units to %s which has %d units" % (attacker,attackfrom,g.locations.get(attackfrom)[1],attackto,g.locations.get(attackto)[1]))

            result = g.attack(attacker,attackfrom,attackto)

            app.session.publish(mount(gid), 'attack', attackfrom, attackto, result[0], result[1])
                # results are losses from attacker and defender

            if g.hasWon(uid):
                app.session.publish(mount(gid),'gamefinished','The Game Has Ended',gid,uid)

            if debug >3: log.msg("Info: Units per country after attack - %s %d, %s %d" % (attackfrom,g.locations.get(attackfrom)[1],attackto,g.locations.get(attackto)[1]))

            # if a country has been conquered
            if g.locations[attackto][1] <1:
                g.locations[attackto][0] = attackfrom	# set new owner of country

@app.register('com.example.gamechat')
def gc(gid,uname,msg):
            ''' sends an ingame message to all the clients within that selected game
            gamechat;<username>;<message>
            '''
            if not gameExists(gid):
                return "Error: Game does not exist"

            app.session.publish(mount(gid), 'gamechat', uname, msg)



@app.register('com.example.reinforce')
def rf(uid,gid,cfrom,cto,units):
            '''reinforces a countries army from an adjacent country
            reinforce;<sessionid>;<gameid>;<countryfrom>,<countryto>,<units>
            '''
            if not playerExists(uid):
                return "Error: You are not a registered user"
            if not gameExists(gid):
                return "Error: Game does not exist"
            g = gById[gid]
            p = pById[uid]
            if debug >3: log.msg("Info: received reinforce by %s from %s to %s of amount %s" % (p.name,cfrom,cto,units))
            reinforced = g.reinforce(uid,cfrom,cto,units)
            if reinforced:
                    app.session.publish(mount(gid), 'reinforce', cfrom, cto, units)
            else:
                return "Error: cannot reinforce, something went wrong"

@app.register('com.example.placeunit')
def pu(uid,gid,country):
            '''	adds an army unit to the specified country from a players reinforcements
                note - remove useless number of units as it's always one
            '''
            if not playerExists(uid):
                return "Error: You are not a registered user"
            if not gameExists(gid):
                return "Error: Game does not exist"
            g = gById[gid]
            p = pById[uid]
            if debug > 3: log.msg("Info: Received unit placement by %s on %s" % (p.name,country))
            placed = g.placearmy(uid,country)
            if not placed:
                log.msg("placeunit message not returned to client")
            else:
                app.session.publish(mount(gid), 'placeunit', country, g.currentPlayer)
                log.msg("placeunit message returned to client")

@app.register('com.example.turnend')
def te(uid,gid):
            '''return the next user in the list'''
            if debug > 3:
                log.msg("turnEnd received from player %s  " % uid)
            g = gById[gid]
            if not playerExists(uid):
                return "Error: You are not a registered user"
            if not gameExists(gid):
                return "Error: Game does not exist"

            card = g.endTurn()
            if card >= 0:
                app.session.publish(mount(gid),'recieveCard',uid,card)

            #['turn',nextPlayer,state,newArmies]
            app.session.publish(mount(gid), 'turn', g.currentPlayer, g.state, g.currentPlayerReinforcements())
            if debug > 3: log.msg("Info: Turn end message sent")

@app.register('com.example.gamelist')
def gl(uid):
            ''' user requst list of games
            returns results directly to client
            [ <list type>, <list element1>, ..., <list elemnt n> ]
            if player made a game, they are returned a mygame list
            otherwise they get a gamelist list
            '''
            if not playerExists(uid):
                return "Error: You are not a registered user"
            out =[]
            mygame= False
            for x in gById.values():
                if x.owner==uid:				# check if player has created a game already
                    mygame=x
                    continue					# skip adding this game to list as it's the users game
                out.append(x.gid)
                out.append(x.name)

            if mygame:								# send player their game id
                return ['mygame', mygame.gid, mygame.name]
                if debug >2: log.msg("Warn: User %s requested gamelist so resent mygame" % pById[uid].name)
            else:
                return ["gamelist"].extend(out)

@app.register('com.example.gamestate')
def resendGameState(uid,gid):
    '''sends current gamestate on request, useful for reconnecting clients that are in game

    '''
    if not playerExists(uid):
        return "Error: You are not a registered user"
    if not gameExists(gid):
        return "Error: Game does not exist"
    g = gById[gid]
    if len(g.players.keys()) ==0:
        return					# can't send gamestate to no users
    clist = g.locations.keys()
    clist.sort()
    out = ""
    for x in clist:
        out += x + "," + g.locations.get(x)[0] +"," + str(g.locations.get(x)[1]) + ";"
    for p in g.players.keys():
        c = clientById.get(p)
        if c != None:
            c.sendMessage("gamemap;%s" % out)
    if debug>3: log.msg("Info: sent gamemap to remaining users in game %s" % str(gid))

@app.register('com.example.tradecards')
def tradeCards(uid,gid,cards):
    if debug > 3: log.msg("trading cards msg recieved by risky.py");
    g = gById[gid]
    units = g.processTradeCards(uid,cards)
    app.session.publish(mount(gid), 'recieveReinforcements', uid, units)


def cullPlayers():
        timenow = time.time()
        for p in pById.values():
            uid = p.sid
            if (timenow-p.lastseen) > 300:		# if player has been idle for more than 5 minutes
                if debug >2: log.msg("Warn: Player %s id %s is idle past timeout and is being culled" % (p.name,uid))
                for g in gById.values():
                    if uid in g.players:
                        g.cullPlayer(uid)		# cull player from any game they are in
                        resendGameState(g.gid)
                del pById[id]


if __name__ == "__main__":

    log.startLogging(sys.stdout)
    app.run("ws://localhost:8080", "realm1", standalone = True, debug = False, debug_app = False, debug_wamp = False)