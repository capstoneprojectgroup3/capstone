from twisted.python import log
import time
import random
from random import shuffle
from risky import *
from player import *
gameid = 64107	# initial game id

class game:
    '''Class for each game, multiple games can run concurrently'''
    def __init__(self,name="",privacy=False,owner=None):
        global gameid
        self.players = []		# [uid1, .., uidn]
        self.turnOrder = []		# [sid,sid] generated during start method by generateTurnOrder method
        self.defaultLocations = [["alaska", "", 1], ["northernterritory", "", 1], ["ontario", "", 1], ["easternrussia", "", 1], ["quebec", "", 1], ["easternunitedstates", "", 1], ["centralamerica", "", 1], ["venezuela", "", 1], ["peru", "", 1], ["brazil", "", 1], ["argentina", "", 1], ["greenland", "", 1], ["iceland", "", 1], ["greatbritain", "", 1], ["westerneurope", "", 1], ["northerneurope", "", 1], ["southerneurope", "", 1], ["scandinavia", "", 1], ["kamchatka", "", 1], ["japan", "", 1], ["newguinea", "", 1], ["indonesia", "", 1], ["australia", "", 1], ["newzealand", "", 1], ["madagascar", "", 1], ["southafrica", "", 1], ["northafrica", "", 1], ["egypt", "", 1], ["eastafrica", "", 1], ["congo", "", 1], ["russia", "", 1], ["middleeast", "", 1], ["afghanistan", "", 1], ["ural", "", 1], ["yakutsk", "", 1], ["mongolia", "", 1], ["siberia", "", 1], ["india", "", 1], ["china", "", 1], ["thailand", "", 1], ["westernunitedstates", "", 1], ["alberta", "", 1], ]
        self.locations = {}
        self.gid = str(gameid)
        gameid +=1
        self.name = name
        self.password = privacy
        self.state = 0		# waiting for players, 1 = place units mode, 2 = ingame, 3 = game completed - additional states could be used for which player input is required from etc
                            # 4 is in-game place units, for the start of each turn
        self.owner = owner	# id of player that created the game
        self.currentPlayer = None
        self.cards = [['wild', '3'], ['alaska', 0], ['northernterritory', 1], ['ontario', 2], ['easternrussia', 0], ['quebec', 1], ['easternunitedstates', 2], ['centralamerica', 0], ['venezuela', 1], ['peru', 2], ['brazil', 0], ['argentina', 1], ['greenland', 2], ['iceland', 0], ['greatbritain', 1], ['westerneurope', 2], ['northerneurope', 0], ['southerneurope', 1], ['scandinavia', 2], ['kamchatka', 0], ['japan', 1], ['newguinea', 2], ['indonesia', 0], ['australia', 1], ['newzealand', 2], ['madagascar', 0], ['southafrica', 1], ['northafrica', 2], ['egypt', 0], ['eastafrica', 1], ['congo', 2], ['russia', 0], ['middleeast', 1], ['afghanistan', 2], ['ural', 0], ['yakutsk', 1], ['mongolia', 2], ['siberia', 0], ['india', 1], ['china', 2], ['thailand', 0], ['westernunitedstates', 1], ['alberta', 2]]
        self.availableCards = range(len(self.cards))
        self.cardTradeWorth = [4,6,8,10,12,15]
        self.playersInGame = 1

    def start(self):
        '''Sets game to start mode, generating all the initial player controlled countries list and player turn order'''
        self.assignArmyUnits()
        self.initialise()			# generates game locations and assigns players initial ownership
        self.generateTurnOrder()
        self.state = 1				# proceed to place army mode

    def startingUnits(self):
        #returns the number of units each player should start with
        #return 26
        initialPieces = {2:40,3:35,4:30,5:25,6:20}
        return initialPieces.get(len(self.players))

    def assignArmyUnits(self):
        '''assigns starting army units to players based on how many players there are as per risk rules'''
        global pById
        for p in self.players:
            plr = pById.get(p)
            if p == None:
                log.msg('wtf, player no found')
            plr.reinforcements=self.startingUnits()

    def playerReinforcements(self):
        '''returns a list of reinforcements in order of players'''
        global pById
        out = []
        for i in self.players:
            out.append(pById[i].reinforcements)
        return out

    def generateTurnOrder(self):
        '''generates player turn order, not normally called directly, but called during start method'''
        for user in self.players:
            self.turnOrder.append(user)

        shuffle(self.turnOrder)
        self.currentPlayer = self.turnOrder[0]

    #retrieve the turn order as is stored on the server
    def getTurnOrder(self):
        return self.turnOrder

    def initialise(self):
        '''initialses game locations, called by start method'''
        global pById
        initGame = self
        defaultList = self.defaultLocations

        #makes a list of all available country indexes
        availableCountries = range(len(self.defaultLocations))
        if debug > 3:
            log.msg('Initialising game...')
        #loop while there are countries available
        while len(availableCountries) >= 1:
            #loop through the list of players dividing countries equally
            for player in self.players:
                #if only one remains assign and exit loop
                if len(availableCountries) == 0:
                    break
                #assign country to player then remove that reference from the available list
                ran = random.randint(0, len(availableCountries)-1)
                defaultList[availableCountries[ran]][1] = player
                p = pById[player]
                if not p:
                    log.msg('wth no player %s' % player)
                    break
                p.reinforcements -=1		# reduce available reinforcements
                del availableCountries[ran]

        if debug > 3:
            log.msg('countries assigned')
        #convert the output into a dictionary so that values can be obtained faster, as to avoid
        #iterating through each individual list
        storageDictionary = {}
        for items in defaultList:
            storageDictionary[items[0]] = [items[1], items[2]]

        self.locations = storageDictionary
        log.msg("generated initial gamestate for game: %s" % self.gid)


    def attack(self,uid,attackfrom,attackto):
        ''' performs attack move
            attackfrom and attackto are strings of country names
            returns a pair of string numbers in a list
            [<attacker Losses>, <defender Losses>]
        '''
        #get current game state values
        curState = self.locations

        #determine the attack and defending amounts
        attackingAmount = self.locations.get(attackfrom)[1]-1
        defendingAmount = self.locations.get(attackto)[1]

        #generate the combat dice lists for each side
        attackingRolls = self.rolldice(attackingAmount, "attack")
        defendingRolls = self.rolldice(defendingAmount, "defend")

        #keep track of losses on each side
        attackerLosses = 0
        defenderLosses = 0

        while len(defendingRolls) >0:
            #depending on outcome increment loss totals
            if attackingRolls[0] > defendingRolls[0]:
                defenderLosses += 1
            else:
                attackerLosses += 1

            #remove checked totals from lists and continue until battle is completed
            del attackingRolls[0]
            del defendingRolls[0]

        self.locations.get(attackfrom)[1] -= attackerLosses
        self.locations.get(attackto)[1] -= defenderLosses

        # if attacker conquers country
        if self.locations.get(attackto)[1] < 1:
            amountToMove = self.locations.get(attackfrom)[1] - 1
            newOwner = self.locations.get(attackfrom)[0]
            pById[uid].conquered = True

            self.locations[attackto] = [newOwner, amountToMove]
            self.locations[attackfrom][1] = 1


        return [str(attackerLosses), str(defenderLosses)]

    def placearmy(self,uid,country,amount=1):
        '''places army units on player owned countries, only callable in place army mode (game state 1 or 4)'''
        if not (self.state == 1 or self.state == 4):
            log.msg("Error: %s attempted to place army unit while not in place army mode, current game state: %d" % (uid,self.state))
            return False
        else:
            self.currentPlayer = self.nextPlayer(self.currentPlayer)
            if self.locations.get(country)[0] == uid:		# check if country owned by player
                self.locations.get(country)[1] = int(self.locations.get(country)[1]) + 1
                pById.get(uid).reinforcements -= 1	# reduce available units by amount places
                if debug>3:
                    log.msg("Info: Placed %d units at %s, there are now %d units there" % (int(amount),country,self.locations.get(country)[1]))
                    log.msg("Info: Player %s id (%s) now has %s reinforcements" % (str(pById.get(uid).name),str(uid),str(pById.get(uid).reinforcements)))
                if not self.playersHaveReinforcements():
                    self.state = 2					# change game status once all reinforcements placed
                return True
            else:
                log.msg("Error: %s attempted to place army unit at %s which is owned by %s" % (uid,country,self.locations.get(country)[0]))
                return False

    #this deals with end of turn reinforcement by a player
    #updates the game state accordingly and returns it to each user
    def reinforce(self,uid,countryfrom,countryto,amount=1):
        amount = int(amount)
        if not self.locations.get(countryfrom)[0]==uid:
            log.msg("Error: %s cannot reinforce from %s because they do not own it" % (uid,countryfrom))
            return False
        if not self.locations.get(countryto)[0]==uid:
            log.msg("Error: %s cannot reinforce	 %s because they do not own it" % (uid,countryto))
            return False
        if not int(self.locations.get(countryfrom)[1])>1:
            log.msg("Error: %s cannot reinforce from %s because there is only 1 unit there" % (uid,countryfrom))
            return False
        self.locations.get(countryfrom)[1] = int(self.locations.get(countryfrom)[1]) - amount	# take one from first country
        self.locations.get(countryto)[1] = int(self.locations.get(countryto)[1]) + amount			# give it to second country
        log.msg("%s reinforced %s from %s, there are now %d units at %s" % (uid,countryto,countryfrom,int(self.locations.get(countryto)[1]),countryto))
        return True

    def nextPlayer(self,uid):
        '''returns the id of the next player'''
        log.msg("checking next player")
        if self.state == 4:
            log.msg("in phase 4 (ingame placeunits) so nextplayer should be %s" %uid)
            return uid #if in in-game place units mode, just stay the same turn
        if self.turnOrder.index(uid) == len(self.turnOrder)-1:
            return self.turnOrder[0]
        else:
            return self.turnOrder[self.turnOrder.index(uid)+1]


    def hasWon(self,uid):
        '''check to see if a player has won the game'''
        if all(x[0]==uid for x in self.locations.values()):		# all countries owned by player
            return True
        #if not return false, the game continues
        else:
            return False

    def rolldice(self,armieSize, type):
        rollAmount = 0
        #type represents attack or defend
        if (armieSize == 1) and (type == "attack"):
            rollAmount = 1

        elif (armieSize == 2) and (type == "attack"):
            rollAmount = 2

        elif (armieSize > 2) and (type == "attack"):
            rollAmount = 3

        elif (armieSize == 1) and (type == "defend"):
            rollAmount = 1

        elif (armieSize == 2) and (type == "defend"):
            rollAmount = 2

        elif (armieSize > 2) and (type == "defend"):
            rollAmount = 2

        diceroll = []

        #roll the random dice rolls
        for i in range(rollAmount):
            diceroll.append(random.randint(1,6))

        #sort from highest to lowest
        diceroll.sort(reverse=True)

        return diceroll

    def cullPlayer(self,id):
        if not id in self.players:
            return				# cant cull non-existant player
        if not len(self.players)<=2:
            others = [x for x in self.players if x != id]
            owned = [x for x in self.locations.keys() if self.locations.get(x)[0] == id]
            if debug >2: log.msg("Warn: Player %s was in game %s now reassigning their owned territories to remaining players" % (id,str(self.gid)))
            shuffle(owned)
            shuffle(others)
            i=0
            while len(owned) > 0:
                c = owned.pop()
                p = others[i]
                c2 = self.locations.get(c)
                c2[0] = p
                c2[1] = 1
                if debug > 3: log.msg("Info: giving %s to %s" % (c,others[i]))
                i+=1
                if i == len(others):
                    i=0
            del self.players[id]
            if debug >2: log.msg("Warn: Culled user id %s from game %s" % (id,str(self.gid)))
        else:
            self.endGame()
        return True

    def endTurn(self):

        '''	set currentPlayer to the next player and assigns extra units for the next player
            extra units are based on countries owned
            x<11=3		11<=x<14=4		14<=x=5		as per risk rules
        '''
        log.msg("######PROCESSING ENDTURN (in game.py)#############")

        card = -1
        p = pById[self.currentPlayer]
        if p.conquered:
            card = random.choice(self.availableCards)		# player earns a card if they've conquered a country at turn end
            p.cards.append(card)
            self.availableCards.remove(card)
            p.conquered = False						# reset country conquered flag

        self.currentPlayer = self.nextPlayer(self.currentPlayer)
        log.msg("next player: %s" % self.currentPlayer)

        owned = len([x for x in self.locations.keys() if self.locations.get(x)[0] == self.currentPlayer])

        log.msg("players owns %i territories" % owned)

        extraunits = 3
        additionalunits = int(owned/3)
        extraunits += additionalunits
        #if owned >=14: extraunits +=1
        #if owned >=17: extraunits +=1

        pById[self.currentPlayer].reinforcements += extraunits
        log.msg("players gets %i extra units" % extraunits)
        self.state = 4 #set to in-game placeUnits
        log.msg("######FINISHED PROCESSING ENDTURN (in game.py)#############")

        return card

    def processTradeCards(self,uid,cards):
        log.msg("trading cards msg recieved by game");
        p = pById[uid]
        units = 0
        #determine appropriate number of units
        if p.cardTrades <= 5: units = self.cardTradeWorth[p.cardTrades]
        else: units = 15 + 5 * (p.cardTrades - 5)
        p.reinforcements += units
        p.cardTrades += 1
        #return cards to pool
        for x in cards:
            self.availableCards.append(x)
        log.msg("for cards %i %i %i, determined %i units" % (cards[0],cards[1],cards[2],units));
        return units


    def currentPlayerReinforcements(self):
        return pById[self.currentPlayer].reinforcements

    def playersHaveReinforcements(self):
        '''returns a boolean True if at least 1 player has >0 reinforcements'''
        global pById
        for p in [pById[x] for x in self.players]:
            if p.reinforcements >0:
                return True
        return False

    def endGame(self):
        self.lobby.gameOver(self.gid)