<?php
/**
 * Opauth example
 * 
 * This is an example on how to instantiate Opauth
 * For this example, Opauth config is loaded from a separate file: opauth.conf.php
 * 
 */

/**
 * Define paths
 */
define('CONF_FILE', dirname(__FILE__).'/'.'opauth.conf.php');
define('OPAUTH_LIB_DIR', dirname(__FILE__).'/lib/Opauth/');

/**
* Load config
*/
if (!file_exists(CONF_FILE)){
	trigger_error('Config file missing at '.CONF_FILE, E_USER_ERROR);
	exit();
}
require CONF_FILE;

/**
 * Instantiate Opauth with the loaded config
 */
require OPAUTH_LIB_DIR.'Opauth.php';
$Opauth = new Opauth( $config );
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Global Thermo-Nuclear World Domination</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<link type="text/css" rel="stylesheet" href="main.css"/>
		<style>
			body {
				color: #585858;
				background-color: #d9dbda;
				margin: 0px;
				overflow: hidden;
			}
			#info {
				position: absolute;
				top: 50px;
				left: 50%;
				border-radius: 10px;
				background-color: #ffffff;
				height: 300px;
				width: 400px;
				padding: 5px;
				margin: 0 0 0 -200px;
				font-family:'Titillium Web', sans-serif;
				font-size: 13px;
				text-align: center;
				font-weight: bold;
			}
			a {
				color: #fff;
			}
		</style>
	</head>

	<body>
		<div id="container"></div>
		<div id="info" style="padding-top: 50px">
		<h1>Futuristic War Game</h1><br /><br /><br />
		Please login to continue<br /><br />
		<a href="/authtest/facebook"><img src="images/fbimg.png" /></a><br /><br />
		<a href="/authtest/google"><img src="images/gimg.png" /></a>
		</div>

</body>

</html>